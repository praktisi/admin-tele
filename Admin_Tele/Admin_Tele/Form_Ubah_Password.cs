﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Admin_Tele
{
    public partial class Form_Ubah_Password : Form
    {
        MySqlConnection conn = new MySqlConnection(Constanta.conString);
        MySqlCommand cmd;
        MySqlDataAdapter adpt;
        string query;

        public Form_Ubah_Password()
        {
            InitializeComponent();
        }

        private void UbahPassword(string pwd, string uName)
        {
            query = "UPDATE tb_admin SET password_admin ='" + pwd + "' WHERE username_admin = '" + uName + "'";
            cmd = new MySqlCommand(query, conn);

            try
            {
                conn.Open();

                adpt = new MySqlDataAdapter(cmd);
                adpt.UpdateCommand = conn.CreateCommand();
                adpt.UpdateCommand.CommandText = query;

                if (cmd.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("Password telah berhasil diubah");
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ClearText()
        {
            TxtBox_Username.Text = "";
            TxtBox_Password.Text = "";
            TxtBox_PwdBaru.Text = "";
            GrpBox_UbahPassword.Visible = false;
            GrpBox_KonfirmPassword.Enabled = true;
        }

        private void Btn_Konfirmasi_Click(object sender, EventArgs e)
        {
            if(TxtBox_Password.Text == Login.password && TxtBox_Username.Text == Login.uName)
            {
                GrpBox_KonfirmPassword.Enabled = false;
                GrpBox_UbahPassword.Visible = true;
                TxtBox_PwdBaru.Focus();
            }
            else
            {
                MessageBox.Show("Maaf password atau username yang anda masukan salah");
            }
        }

        bool isChange = false;
        string newPassword;

        private void Btn_Ubah_Click(object sender, EventArgs e)
        {
            if(!isChange)
            {
                newPassword = TxtBox_PwdBaru.Text;
                isChange = true;
                TxtBox_PwdBaru.Text = "";
                MessageBox.Show("konfirmasi ulang password baru anda");
                TxtBox_PwdBaru.Focus();
            }
            else
            {
                if (MessageBox.Show("Anda yakin akan mengubah password anda?", "Ubah Password",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    if(TxtBox_PwdBaru.Text == newPassword)
                    {
                        UbahPassword(TxtBox_PwdBaru.Text, TxtBox_Username.Text);
                        Login.password = TxtBox_PwdBaru.Text;
                        ClearText();
                        newPassword = null;
                        isChange = false;
                    }
                    else
                    {
                        MessageBox.Show("password baru yang anda inputkan salah, harap input ulang password baru anda");
                        TxtBox_PwdBaru.Text = "";
                        newPassword = null;
                        isChange = false;
                    }
                }
            }
        }

        private void Btn_Batal_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Apakah anda tidak akan mengubah password anda?", "Ubah Password",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                ClearText();
                newPassword = null;
                isChange = false;
            }
        }

        private void Form_Ubah_Password_FormClosing(object sender, FormClosingEventArgs e)
        {
            Main.isChildFormAppeared = !Main.isChildFormAppeared;
        }
    }
}
