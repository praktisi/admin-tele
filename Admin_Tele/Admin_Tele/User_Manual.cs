﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Admin_Tele
{
    public partial class User_Manual : Form
    {
        public User_Manual()
        {
            InitializeComponent();
        }

        private void User_Manual_FormClosing(object sender, FormClosingEventArgs e)
        {
            Main.isUserManualAppeared = !Main.isUserManualAppeared;
        }
    }
}
