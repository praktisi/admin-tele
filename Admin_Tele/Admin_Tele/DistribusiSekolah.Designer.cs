﻿namespace Admin_Tele
{
    partial class DistribusiSekolah
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Panel_DaftarSekolah = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Cmb_TahunPromo = new System.Windows.Forms.ComboBox();
            this.Btn_Submit = new System.Windows.Forms.Button();
            this.Cmb_NamaMarketing = new System.Windows.Forms.ComboBox();
            this.Cmb_NamaTele = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Panel_DaftarSekolah
            // 
            this.Panel_DaftarSekolah.AutoScroll = true;
            this.Panel_DaftarSekolah.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_DaftarSekolah.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Panel_DaftarSekolah.Location = new System.Drawing.Point(12, 30);
            this.Panel_DaftarSekolah.Name = "Panel_DaftarSekolah";
            this.Panel_DaftarSekolah.Size = new System.Drawing.Size(282, 330);
            this.Panel_DaftarSekolah.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(16, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Daftar Sekolah";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Tahun Promosi";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 16);
            this.label3.TabIndex = 3;
            this.label3.Text = "Nama Marketing";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 111);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(135, 16);
            this.label4.TabIndex = 4;
            this.label4.Text = "Nama Telemarketing";
            // 
            // Cmb_TahunPromo
            // 
            this.Cmb_TahunPromo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_TahunPromo.FormattingEnabled = true;
            this.Cmb_TahunPromo.Location = new System.Drawing.Point(147, 36);
            this.Cmb_TahunPromo.Name = "Cmb_TahunPromo";
            this.Cmb_TahunPromo.Size = new System.Drawing.Size(121, 21);
            this.Cmb_TahunPromo.TabIndex = 0;
            this.Cmb_TahunPromo.SelectedIndexChanged += new System.EventHandler(this.Cmb_TahunPromo_SelectedIndexChanged);
            // 
            // Btn_Submit
            // 
            this.Btn_Submit.Enabled = false;
            this.Btn_Submit.Location = new System.Drawing.Point(550, 337);
            this.Btn_Submit.Name = "Btn_Submit";
            this.Btn_Submit.Size = new System.Drawing.Size(75, 23);
            this.Btn_Submit.TabIndex = 3;
            this.Btn_Submit.Text = "Simpan";
            this.Btn_Submit.UseVisualStyleBackColor = true;
            this.Btn_Submit.Click += new System.EventHandler(this.Btn_Submit_Click);
            // 
            // Cmb_NamaMarketing
            // 
            this.Cmb_NamaMarketing.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_NamaMarketing.Enabled = false;
            this.Cmb_NamaMarketing.FormattingEnabled = true;
            this.Cmb_NamaMarketing.Location = new System.Drawing.Point(147, 72);
            this.Cmb_NamaMarketing.Name = "Cmb_NamaMarketing";
            this.Cmb_NamaMarketing.Size = new System.Drawing.Size(161, 21);
            this.Cmb_NamaMarketing.TabIndex = 1;
            this.Cmb_NamaMarketing.SelectedIndexChanged += new System.EventHandler(this.Cmb_NamaMarketing_SelectedIndexChanged);
            // 
            // Cmb_NamaTele
            // 
            this.Cmb_NamaTele.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_NamaTele.Enabled = false;
            this.Cmb_NamaTele.FormattingEnabled = true;
            this.Cmb_NamaTele.Location = new System.Drawing.Point(147, 110);
            this.Cmb_NamaTele.Name = "Cmb_NamaTele";
            this.Cmb_NamaTele.Size = new System.Drawing.Size(161, 21);
            this.Cmb_NamaTele.TabIndex = 2;
            this.Cmb_NamaTele.SelectedIndexChanged += new System.EventHandler(this.Cmb_NamaTele_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.Cmb_NamaTele);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.Cmb_NamaMarketing);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.Cmb_TahunPromo);
            this.groupBox1.Location = new System.Drawing.Point(311, 22);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(314, 150);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            // 
            // DistribusiSekolah
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Admin_Tele.Properties.Resources.Background5;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(641, 372);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Btn_Submit);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Panel_DaftarSekolah);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "DistribusiSekolah";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Distribusi Sekolah";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DistribusiSekolah_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel Panel_DaftarSekolah;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox Cmb_TahunPromo;
        private System.Windows.Forms.Button Btn_Submit;
        private System.Windows.Forms.ComboBox Cmb_NamaMarketing;
        private System.Windows.Forms.ComboBox Cmb_NamaTele;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}