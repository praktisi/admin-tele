﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Admin_Tele
{
    public partial class FormChartReportPermarketing : Form
    {
        public FormChartReportPermarketing()
        {
            InitializeComponent();
        }
        private void FillChartBar(List<string> kategori, List<int> hasilKategori)
        {
            try
            {
                chart1.Series.Clear();
                chart1.Titles.Clear();
                chart1.Series.Add(Cmb_SesiKe.Text);
                chart1.Titles.Add("Hasil Peneleponan " + Cmb_SesiKe.Text);
                for (int i = 0; i < kategori.Count; i++)
                {
                    chart1.Series[0].Points.AddXY(kategori[i], hasilKategori[i]);
                }
                chart1.ChartAreas[0].AxisX.Minimum = 0;
                chart1.ChartAreas[0].AxisX.Interval = 1;
                chart1.ChartAreas[0].AxisY.Maximum = hasilKategori.Max();
            }
            catch (Exception)
            {
                MessageBox.Show("Tidak ada peneleponan yang diinput pada sesi ini");
            }
        }

        private void FillChartPie(List<string> kategori, List<int> hasilKategori)
        {
            chart2.Series.Clear();
            chart2.Titles.Clear();
            chart2.Legends.Clear();
            chart2.Series.Add(Cmb_SesiKe.Text);
            chart2.Titles.Add("Hasil Peneleponan " + Cmb_SesiKe.Text);
            chart2.Legends.Add("Legend1");
            chart2.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            for (int i = 0; i < kategori.Count; i++)
            {
                chart2.Series[0].Points.AddXY(kategori[i], hasilKategori[i]);
            }

            // pengkodean #PERCENT{P2} untuk label (Yvalue Members) untuk menampilkan secara persentase
            chart2.Series[0].Label = "#PERCENT{P2}";

            // pengkodean #VALX untuk legend agar menampilkan data awal (nama_barang)
            chart2.Series[0].LegendText = "#VALX";

            // set text dihilangkan untuk data yang memiliki nilai = 0
            for (int i = 0; i < kategori.Count; i++)
            {
                if (hasilKategori[i] == 0)
                {
                    chart2.Series[0].Points[i]["PieLabelStyle"] = "Disabled";
                }
            }
        }

        private void Cmb_SesiKe_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Cmb_SesiKe.Text != null)
            {
                if (Cmb_SesiKe.SelectedIndex == 0)
                {
                    FillChartBar(FormReportPerMarketing.katjawSesi1, FormReportPerMarketing.hasilSesi1);
                    FillChartPie(FormReportPerMarketing.katjawSesi1, FormReportPerMarketing.hasilSesi1);
                }
                else if (Cmb_SesiKe.SelectedIndex == 1)
                {
                    FillChartBar(FormReportPerMarketing.katjawSesi2, FormReportPerMarketing.hasilSesi2);
                    FillChartPie(FormReportPerMarketing.katjawSesi2, FormReportPerMarketing.hasilSesi2);
                }
                else if (Cmb_SesiKe.SelectedIndex == 2)
                {
                    FillChartBar(FormReportPerMarketing.katjawSesi3, FormReportPerMarketing.hasilSesi3);
                    FillChartPie(FormReportPerMarketing.katjawSesi3, FormReportPerMarketing.hasilSesi3);
                }
                else if (Cmb_SesiKe.SelectedIndex == 3)
                {
                    FillChartBar(FormReportPerMarketing.katjawSesi4, FormReportPerMarketing.hasilSesi4);
                    FillChartPie(FormReportPerMarketing.katjawSesi4, FormReportPerMarketing.hasilSesi4);
                }
                else if (Cmb_SesiKe.SelectedIndex == 4)
                {
                    FillChartBar(FormReportPerMarketing.katjawSesi5, FormReportPerMarketing.hasilSesi5);
                    FillChartPie(FormReportPerMarketing.katjawSesi5, FormReportPerMarketing.hasilSesi5);
                }
                else
                {
                    FillChartBar(FormReportPerMarketing.katjawTotal, FormReportPerMarketing.sumPerkatjaw);
                    FillChartPie(FormReportPerMarketing.katjawTotal, FormReportPerMarketing.sumPerkatjaw);
                }
            }
        }

        private void FormChartReportPermarketing_FormClosing(object sender, FormClosingEventArgs e)
        {
            FormReportPerMarketing.isChartAppeared = !FormReportPerMarketing.isChartAppeared;
        }
    }
}
