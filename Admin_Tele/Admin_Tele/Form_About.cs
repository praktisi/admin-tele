﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Admin_Tele
{
    public partial class Form_About : Form
    {
        public Form_About()
        {
            InitializeComponent();
        }

        private void Btn_OK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form_About_FormClosing(object sender, FormClosingEventArgs e)
        {
            Main.isAboutAppeared = !Main.isAboutAppeared;
        }
    }
}
