﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Microsoft.VisualBasic;

namespace Admin_Tele
{
    public partial class Login : Form
    {
        MySqlConnection connect = new MySqlConnection(Constanta.conString);
        MySqlCommand command;
        MySqlDataReader reader;

        public static string uName;
        public static string password;

        public Login()
        {
            InitializeComponent();
            Constanta.SettingServer();
        }

        private bool SignIn(string Uname, string pass)
        {
            connect = new MySqlConnection(Constanta.conString);
            try
            {
                string query = "SELECT * FROM tb_admin WHERE username_admin ='" + Uname + "' AND password_admin ='" + pass + "'";
                command = new MySqlCommand(query, connect);
                connect.Open();
                reader = command.ExecuteReader();
                int count = 0;
                while (reader.Read())
                {
                    count = count + 1;
                }
                if (count == 1)
                {
                    uName = Uname;
                    password = pass;
                    MessageBox.Show("Selamat " + Waktu() + " " + txtBox_Uname.Text);
                    return true;
                }
                else 
                {
                    return false;
                }

                connect.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        private string Waktu()
        {
            string hasil = "";

            if (DateTime.Now.Hour >= 6 && DateTime.Now.Hour < 10)
                hasil = "pagi";
            else if (DateTime.Now.Hour >= 10 && DateTime.Now.Hour < 15)
                hasil = "siang";
            else if (DateTime.Now.Hour >= 15 && DateTime.Now.Hour < 18)
                hasil = "sore";
            else if (DateTime.Now.Hour >= 18)
                hasil = "malam";
            else
                hasil = "";

            return hasil;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // button Masuk
            btn_signIn();
            
        }

        private void btn_signIn()
        {
            if (SignIn(txtBox_Uname.Text, txtbox_Password.Text))
            {
                txtBox_Uname.Clear();
                txtbox_Password.Clear();

                Main halMain = new Main();
                halMain.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Username atau Password anda salah");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // button Exit
            Application.Exit();
        }

        private void btn_Settings_Click(object sender, EventArgs e)
        {
            string x = Interaction.InputBox("Developer Team Account : ", "Entering To Configuration", "");
            if (x == "UPT Pengembangan Sisfo Praktisi")
            {
                FormSettings fset = new FormSettings();
                fset.ShowDialog();
            }
            else
            {
                MessageBox.Show("Configuration is Denied");
            }
        }

        private void txtBox_Uname_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btn_signIn();
            }
        }

        private void txtbox_Password_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btn_signIn();
            }
        }

        private void Login_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        public static bool isHelpAppeared = false;
        private void btn_Help_Click(object sender, EventArgs e)
        {
            if(!isHelpAppeared)
            {
                isHelpAppeared = !isHelpAppeared;

                Form_Help_Login helpLogin = new Form_Help_Login();
                helpLogin.Show();
            }
        }
    }
}
