﻿namespace Admin_Tele
{
    partial class TelemarketingManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GrpBox_DGView_Tele = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.GrpBox_MarketingLeader = new System.Windows.Forms.GroupBox();
            this.TxtBox_IDMarketing = new System.Windows.Forms.TextBox();
            this.Cmb_MarketingLeader = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.GrpBox_DataTele = new System.Windows.Forms.GroupBox();
            this.TxtBox_Password = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtBox_Username = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtBox_NamaTele = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtBox_IDTele = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.Btn_Hapus = new System.Windows.Forms.Button();
            this.Btn_Ubah = new System.Windows.Forms.Button();
            this.Btn_Batal = new System.Windows.Forms.Button();
            this.Btn_Input = new System.Windows.Forms.Button();
            this.GrpBox_DGView_Tele.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.GrpBox_MarketingLeader.SuspendLayout();
            this.GrpBox_DataTele.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // GrpBox_DGView_Tele
            // 
            this.GrpBox_DGView_Tele.BackColor = System.Drawing.Color.Transparent;
            this.GrpBox_DGView_Tele.Controls.Add(this.dataGridView1);
            this.GrpBox_DGView_Tele.Location = new System.Drawing.Point(12, 12);
            this.GrpBox_DGView_Tele.Name = "GrpBox_DGView_Tele";
            this.GrpBox_DGView_Tele.Size = new System.Drawing.Size(703, 258);
            this.GrpBox_DGView_Tele.TabIndex = 0;
            this.GrpBox_DGView_Tele.TabStop = false;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(6, 19);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(691, 233);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.TabStop = false;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // GrpBox_MarketingLeader
            // 
            this.GrpBox_MarketingLeader.BackColor = System.Drawing.Color.Transparent;
            this.GrpBox_MarketingLeader.Controls.Add(this.TxtBox_IDMarketing);
            this.GrpBox_MarketingLeader.Controls.Add(this.Cmb_MarketingLeader);
            this.GrpBox_MarketingLeader.Controls.Add(this.label2);
            this.GrpBox_MarketingLeader.Controls.Add(this.label1);
            this.GrpBox_MarketingLeader.Location = new System.Drawing.Point(12, 276);
            this.GrpBox_MarketingLeader.Name = "GrpBox_MarketingLeader";
            this.GrpBox_MarketingLeader.Size = new System.Drawing.Size(310, 84);
            this.GrpBox_MarketingLeader.TabIndex = 1;
            this.GrpBox_MarketingLeader.TabStop = false;
            // 
            // TxtBox_IDMarketing
            // 
            this.TxtBox_IDMarketing.Enabled = false;
            this.TxtBox_IDMarketing.Location = new System.Drawing.Point(125, 51);
            this.TxtBox_IDMarketing.Name = "TxtBox_IDMarketing";
            this.TxtBox_IDMarketing.Size = new System.Drawing.Size(100, 20);
            this.TxtBox_IDMarketing.TabIndex = 1;
            // 
            // Cmb_MarketingLeader
            // 
            this.Cmb_MarketingLeader.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_MarketingLeader.FormattingEnabled = true;
            this.Cmb_MarketingLeader.Location = new System.Drawing.Point(125, 15);
            this.Cmb_MarketingLeader.Name = "Cmb_MarketingLeader";
            this.Cmb_MarketingLeader.Size = new System.Drawing.Size(175, 21);
            this.Cmb_MarketingLeader.TabIndex = 0;
            this.Cmb_MarketingLeader.SelectedValueChanged += new System.EventHandler(this.Cmb_MarketingLeader_SelectedValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(36, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "ID Marketing";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Marketing Leader";
            // 
            // GrpBox_DataTele
            // 
            this.GrpBox_DataTele.BackColor = System.Drawing.Color.Transparent;
            this.GrpBox_DataTele.Controls.Add(this.TxtBox_Password);
            this.GrpBox_DataTele.Controls.Add(this.label4);
            this.GrpBox_DataTele.Controls.Add(this.TxtBox_Username);
            this.GrpBox_DataTele.Controls.Add(this.label5);
            this.GrpBox_DataTele.Controls.Add(this.TxtBox_NamaTele);
            this.GrpBox_DataTele.Controls.Add(this.label3);
            this.GrpBox_DataTele.Controls.Add(this.TxtBox_IDTele);
            this.GrpBox_DataTele.Controls.Add(this.label6);
            this.GrpBox_DataTele.Enabled = false;
            this.GrpBox_DataTele.Location = new System.Drawing.Point(328, 276);
            this.GrpBox_DataTele.Name = "GrpBox_DataTele";
            this.GrpBox_DataTele.Size = new System.Drawing.Size(387, 166);
            this.GrpBox_DataTele.TabIndex = 2;
            this.GrpBox_DataTele.TabStop = false;
            // 
            // TxtBox_Password
            // 
            this.TxtBox_Password.Location = new System.Drawing.Point(147, 126);
            this.TxtBox_Password.Name = "TxtBox_Password";
            this.TxtBox_Password.Size = new System.Drawing.Size(141, 20);
            this.TxtBox_Password.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(135, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "Nama Telemarketing";
            // 
            // TxtBox_Username
            // 
            this.TxtBox_Username.Location = new System.Drawing.Point(147, 88);
            this.TxtBox_Username.Name = "TxtBox_Username";
            this.TxtBox_Username.Size = new System.Drawing.Size(163, 20);
            this.TxtBox_Username.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(70, 89);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 16);
            this.label5.TabIndex = 4;
            this.label5.Text = "Username";
            // 
            // TxtBox_NamaTele
            // 
            this.TxtBox_NamaTele.Location = new System.Drawing.Point(147, 51);
            this.TxtBox_NamaTele.Name = "TxtBox_NamaTele";
            this.TxtBox_NamaTele.Size = new System.Drawing.Size(201, 20);
            this.TxtBox_NamaTele.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(30, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "ID Telemarketing";
            // 
            // TxtBox_IDTele
            // 
            this.TxtBox_IDTele.Enabled = false;
            this.TxtBox_IDTele.Location = new System.Drawing.Point(147, 15);
            this.TxtBox_IDTele.Name = "TxtBox_IDTele";
            this.TxtBox_IDTele.Size = new System.Drawing.Size(100, 20);
            this.TxtBox_IDTele.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(73, 127);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 16);
            this.label6.TabIndex = 5;
            this.label6.Text = "Password";
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.Transparent;
            this.groupBox4.Controls.Add(this.Btn_Hapus);
            this.groupBox4.Controls.Add(this.Btn_Ubah);
            this.groupBox4.Controls.Add(this.Btn_Batal);
            this.groupBox4.Controls.Add(this.Btn_Input);
            this.groupBox4.Location = new System.Drawing.Point(328, 448);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(387, 49);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            // 
            // Btn_Hapus
            // 
            this.Btn_Hapus.Enabled = false;
            this.Btn_Hapus.Location = new System.Drawing.Point(306, 19);
            this.Btn_Hapus.Name = "Btn_Hapus";
            this.Btn_Hapus.Size = new System.Drawing.Size(75, 23);
            this.Btn_Hapus.TabIndex = 9;
            this.Btn_Hapus.Text = "Hapus";
            this.Btn_Hapus.UseVisualStyleBackColor = true;
            this.Btn_Hapus.Click += new System.EventHandler(this.Btn_Hapus_Click);
            // 
            // Btn_Ubah
            // 
            this.Btn_Ubah.Enabled = false;
            this.Btn_Ubah.Location = new System.Drawing.Point(213, 19);
            this.Btn_Ubah.Name = "Btn_Ubah";
            this.Btn_Ubah.Size = new System.Drawing.Size(75, 23);
            this.Btn_Ubah.TabIndex = 8;
            this.Btn_Ubah.Text = "Ubah";
            this.Btn_Ubah.UseVisualStyleBackColor = true;
            this.Btn_Ubah.Click += new System.EventHandler(this.Btn_Ubah_Click);
            // 
            // Btn_Batal
            // 
            this.Btn_Batal.Enabled = false;
            this.Btn_Batal.Location = new System.Drawing.Point(109, 19);
            this.Btn_Batal.Name = "Btn_Batal";
            this.Btn_Batal.Size = new System.Drawing.Size(75, 23);
            this.Btn_Batal.TabIndex = 7;
            this.Btn_Batal.Text = "Batal";
            this.Btn_Batal.UseVisualStyleBackColor = true;
            this.Btn_Batal.Click += new System.EventHandler(this.Btn_Batal_Click);
            // 
            // Btn_Input
            // 
            this.Btn_Input.Enabled = false;
            this.Btn_Input.Location = new System.Drawing.Point(9, 19);
            this.Btn_Input.Name = "Btn_Input";
            this.Btn_Input.Size = new System.Drawing.Size(75, 23);
            this.Btn_Input.TabIndex = 6;
            this.Btn_Input.Text = "Input";
            this.Btn_Input.UseVisualStyleBackColor = true;
            this.Btn_Input.Click += new System.EventHandler(this.Btn_Input_Click);
            // 
            // TelemarketingManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Admin_Tele.Properties.Resources.Background4;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(729, 509);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.GrpBox_DataTele);
            this.Controls.Add(this.GrpBox_MarketingLeader);
            this.Controls.Add(this.GrpBox_DGView_Tele);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "TelemarketingManager";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Telemarketing Menejer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TelemarketingManager_FormClosing);
            this.GrpBox_DGView_Tele.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.GrpBox_MarketingLeader.ResumeLayout(false);
            this.GrpBox_MarketingLeader.PerformLayout();
            this.GrpBox_DataTele.ResumeLayout(false);
            this.GrpBox_DataTele.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GrpBox_DGView_Tele;
        private System.Windows.Forms.GroupBox GrpBox_MarketingLeader;
        private System.Windows.Forms.GroupBox GrpBox_DataTele;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox TxtBox_IDMarketing;
        private System.Windows.Forms.ComboBox Cmb_MarketingLeader;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtBox_Password;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TxtBox_Username;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TxtBox_NamaTele;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TxtBox_IDTele;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button Btn_Hapus;
        private System.Windows.Forms.Button Btn_Ubah;
        private System.Windows.Forms.Button Btn_Batal;
        private System.Windows.Forms.Button Btn_Input;
    }
}