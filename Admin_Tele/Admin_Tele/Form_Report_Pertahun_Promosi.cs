﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Excel = Microsoft.Office.Interop.Excel;

namespace Admin_Tele
{
    public partial class Form_Report_Pertahun_Promosi : Form
    {
        MySqlConnection conn = new MySqlConnection(Constanta.conString);
        MySqlCommand cmd;
        MySqlDataAdapter adpt;
        string query;
        static string HurufBatasPersen;
        static string HurufBatasTotal;
        static string batasAbjad1;
        static string batasAbjad2;
        List<string> abjad = new List<string>() { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", 
                "K", "L", "M", "N", "O", "P", "Q", "R" ,"S" ,"T" ,"U" ,"V" ,"W" ,"X" , "Y" , "Z", "AA", 
                "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN","AO", 
                "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ", "BA", "BB", "BC", "BD","BE","BF",
                "BG","BH","BI","BJ","BK","BL","BM","BN","BO","BP","BQ","BR","BS","BT","BU","BV","BW","BX","BY","BZ",
                "CA","CB","CC","CD","CE","CF","CG","CH","CI","CJ","CK","CL","CM","CN","CO","CP","CQ","CR","CS","CT",
                "CU","CV","CW","CX","CY","CZ"};
        List<string> idMarketing = new List<string>();
        List<string> idMarketing2 = new List<string>();
        List<string> namaMarketing = new List<string>();
        List<string> namaMarketing2 = new List<string>();
        List<string> namaSekolah = new List<string>();
        List<string> buatBanyakSekolah = new List<string>();
        List<string> buatBanyakSekolah2 = new List<string>();
        List<string> jumlahSekolah = new List<string>();
        List<int> banyakSekolahPermarketing = new List<int>();
        List<int> banyakSekolahPermarketing2 = new List<int>();

        List<int> jumlahAnakPersekolah = new List<int>();
        List<string> sesi1 = new List<string>();
        List<string> sesi2 = new List<string>();
        List<string> sesi3 = new List<string>();
        List<string> sesi4 = new List<string>();
        List<string> sesi5 = new List<string>();

        public static List<string> katjaw = new List<string>();
        public static List<int> sumPerkatjaw = new List<int>();
        public static List<int> hasilSesi1 = new List<int>();
        public static List<int> hasilSesi2 = new List<int>();
        public static List<int> hasilSesi3 = new List<int>();
        public static List<int> hasilSesi4 = new List<int>();
        public static List<int> hasilSesi5 = new List<int>();
        public static List<string> katjawSesi1 = new List<string>();
        public static List<string> katjawSesi2 = new List<string>();
        public static List<string> katjawSesi3 = new List<string>();
        public static List<string> katjawSesi4 = new List<string>();
        public static List<string> katjawSesi5 = new List<string>();
        public static List<string> katjawTotal = new List<string>();

        Excel.Application xlApp;
        Excel.Workbook xlWb;
        Excel.Worksheet xlWs;
            
        public Form_Report_Pertahun_Promosi()
        {
            InitializeComponent();

            FillTahunPromo();
        }

        private void FillTahunPromo()
        {
            query = "SELECT DISTINCT tahun_promosi FROM tb_sesipeneleponan";
            cmd = new MySqlCommand(query, conn);

            try
            {
                conn.Open();

                DataTable dt = new DataTable();
                adpt = new MySqlDataAdapter(cmd);
                adpt.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    Cmb_TahunPromo.Items.Add(r[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void GetKatjaw()
        {
            query = "SELECT id_katjaw, kategori FROM tb_kategorijawaban";
            cmd = new MySqlCommand(query, conn);

            try
            {
                conn.Open();
                DataTable dt = new DataTable();
                adpt = new MySqlDataAdapter(cmd);
                adpt.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    dataGridView1.Rows.Add(r[0].ToString(), r[1].ToString());
                    katjaw.Add(r[1].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void GetMarketing()
        {
            namaMarketing.Clear();
            idMarketing.Clear();

            query = "SELECT id_marketing, nama_marketing FROM tb_marketing";
            cmd = new MySqlCommand(query, conn);

            try
            {
                conn.Open();

                DataTable dt = new DataTable();
                adpt = new MySqlDataAdapter(cmd);
                adpt.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    idMarketing.Add(r[0].ToString());
                    namaMarketing.Add(r[1].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void GetNamaDanJumlahSekolah(string idMar)
        {
            List<string> buatBanyakSekolah = new List<string>();
            query = "SELECT DISTINCT asal_sekolah FROM tb_sesipeneleponan WHERE id_marketing = '" + idMar + "'AND tahun_promosi ='" + Cmb_TahunPromo.Text + "'";
            cmd = new MySqlCommand(query, conn);

            try
            {
                conn.Open();
                DataTable dt = new DataTable();
                adpt = new MySqlDataAdapter(cmd);
                adpt.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    namaSekolah.Add(r[0].ToString());
                    buatBanyakSekolah.Add(r[0].ToString());
                }
                banyakSekolahPermarketing.Add(buatBanyakSekolah.Count);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void DesignGridView()
        {
            dataGridView1.Columns.Clear();

            string[] initAngka = { "1", "2", "3", "4", "5" };
            int initKe = 0;

            dataGridView1.ColumnCount = 2 + namaSekolah.Count * 5;
            dataGridView1.Columns[0].Name = "No.";
            dataGridView1.Columns[0].Width = 30;
            dataGridView1.Columns[1].Name = "Nama Kategori";
            dataGridView1.Columns[1].Width = 255;

            for (int i = 2; i < 2 + namaSekolah.Count * 5; i++)
            {
                dataGridView1.Columns[i].Name = initAngka[initKe];
                dataGridView1.Columns[i].Width = 25;
                initKe++;

                if (initKe == 5)
                {
                    initKe = 0;
                }
            }

            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.AllowUserToResizeColumns = false;

            this.dataGridView1.ColumnHeadersHeight = 23;
            this.dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dataGridView1.ColumnHeadersHeight = this.dataGridView1.ColumnHeadersHeight * 3;
            this.dataGridView1.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
            this.dataGridView1.CellPainting += new DataGridViewCellPaintingEventHandler(dataGridView1_CellPainting);
            this.dataGridView1.Paint += new PaintEventHandler(dataGridView1_Paint);
            this.dataGridView1.Scroll += new ScrollEventHandler(dataGridView1_Scroll);
            this.dataGridView1.ColumnWidthChanged += new DataGridViewColumnEventHandler(dataGridView1_ColumnWidthChanged);
        }

        private void dataGridView1_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            Rectangle rtHeader = this.dataGridView1.DisplayRectangle;
            rtHeader.Height = this.dataGridView1.ColumnHeadersHeight / 2;
            this.dataGridView1.Invalidate(rtHeader);
        }

        private void dataGridView1_Scroll(object sender, ScrollEventArgs e)
        {
            Rectangle rtHeader = this.dataGridView1.DisplayRectangle;
            rtHeader.Height = this.dataGridView1.ColumnHeadersHeight / 2;
            this.dataGridView1.Invalidate(rtHeader);
        }

        private void dataGridView1_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                //Merge nama marketing dengan nama sekolahnya
                for (int i = 0; i < 5 * namaSekolah.Count; )
                {
                    for(int j = 0; j < namaMarketing.Count; j++)
                    {
                        //penempatan teks dimana
                        Rectangle r1 = this.dataGridView1.GetCellDisplayRectangle(i + 2, -1, true);
                        int w2 = this.dataGridView1.GetCellDisplayRectangle(i + 2, -1, true).Width;
                        r1.X += 1;
                        r1.Y += 1;
                        if(banyakSekolahPermarketing[j] > 1)
                        {
                            r1.Width = r1.Width + 99 * banyakSekolahPermarketing[j] + (26 * (banyakSekolahPermarketing[j] - 1));
                        }
                        else
                        {
                            r1.Width = r1.Width + 99;
                        }
                        r1.Height = r1.Height / 3 + 3;
                        e.Graphics.FillRectangle(new SolidBrush(this.dataGridView1.ColumnHeadersDefaultCellStyle.BackColor), r1);
                        StringFormat format = new StringFormat();
                        format.Alignment = StringAlignment.Near;
                        format.LineAlignment = StringAlignment.Center;
                        e.Graphics.DrawString(namaMarketing[j],
                        this.dataGridView1.ColumnHeadersDefaultCellStyle.Font,
                        new SolidBrush(this.dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor),
                        r1, format);
                        i += 5 * banyakSekolahPermarketing[j];
                    }
                }
                

                //Merge nama sekolah dengan sesi
                for (int j = 0; j < 5 * namaSekolah.Count; )
                {
                    // penempatan teks dimana
                    Rectangle r1 = this.dataGridView1.GetCellDisplayRectangle(j + 2, -1, true);
                    int w2 = this.dataGridView1.GetCellDisplayRectangle(j + 2, -1, true).Width;
                    r1.X += 1;
                    r1.Y += 29;
                    r1.Width = r1.Width + 99;
                    r1.Height = r1.Height / 4 + 4;
                    e.Graphics.FillRectangle(new SolidBrush(this.dataGridView1.ColumnHeadersDefaultCellStyle.BackColor), r1);
                    StringFormat format = new StringFormat();
                    format.Alignment = StringAlignment.Near;
                    format.LineAlignment = StringAlignment.Center;
                    e.Graphics.DrawString(namaSekolah[j / 5],
                    this.dataGridView1.ColumnHeadersDefaultCellStyle.Font,
                    new SolidBrush(this.dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor),
                    r1, format);
                    j += 5;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridView1_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.RowIndex == -1 && e.ColumnIndex > -1)
            {
                Rectangle r2 = e.CellBounds;
                r2.Y += e.CellBounds.Height / 2;
                r2.Height = e.CellBounds.Height / 2;
                e.PaintBackground(r2, true);
                e.PaintContent(r2);
                e.Handled = true;
            }
        }

        private void GetBanyakSiswaPersekolah(string asalSekolah)
        {
            query = "SELECT nomer FROM tb_sesipeneleponan WHERE asal_sekolah = '" + asalSekolah + "' ORDER BY nomer DESC LIMIT 0,1";
            cmd = new MySqlCommand(query, conn);

            try
            {
                conn.Open();

                DataTable dt = new DataTable();
                adpt = new MySqlDataAdapter(cmd);
                adpt.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    jumlahAnakPersekolah.Add(int.Parse(r[0].ToString()));
                }

                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void GetValuePersesi(string asalSekolah)
        {
            query = "SELECT sesi_1, sesi_2, sesi_3, sesi_4, sesi_5 FROM tb_sesipeneleponan WHERE asal_sekolah = '" + asalSekolah + "'";
            cmd = new MySqlCommand(query, conn);

            try
            {
                conn.Open();

                DataTable dt = new DataTable();
                adpt = new MySqlDataAdapter(cmd);
                adpt.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    sesi1.Add(r[0].ToString());
                    sesi2.Add(r[1].ToString());
                    sesi3.Add(r[2].ToString());
                    sesi4.Add(r[3].ToString());
                    sesi5.Add(r[4].ToString());
                }

                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void KatjawForCharting(List<string> kategori, List<int> nilai)
        {
            int i = 0;
            while (i < nilai.Count)
            {
                bool isRemove = false;

                if (nilai[i] == 0)
                {
                    nilai.Remove(nilai[i]);
                    kategori.Remove(kategori[i]);
                    isRemove = true;
                }

                if (!isRemove)
                {
                    i++;
                }
            }
        }

        private void ShowData()
        {
            float sumAll = 0;
            List<float> sumSesi1 = new List<float>();
            List<float> sumSesi2 = new List<float>();
            List<float> sumSesi3 = new List<float>();
            List<float> sumSesi4 = new List<float>();
            List<float> sumSesi5 = new List<float>();

            katjaw.Clear();
            sumPerkatjaw.Clear();
            sesi1.Clear();
            sesi2.Clear();
            sesi3.Clear();
            sesi4.Clear();
            sesi5.Clear();
            hasilSesi1.Clear();
            hasilSesi2.Clear();
            hasilSesi3.Clear();
            hasilSesi4.Clear();
            hasilSesi5.Clear();
            namaSekolah.Clear();
            katjawSesi1.Clear();
            katjawSesi2.Clear();
            katjawSesi3.Clear();
            katjawSesi4.Clear();
            katjawSesi5.Clear();
            katjawTotal.Clear();
            jumlahAnakPersekolah.Clear();
            banyakSekolahPermarketing.Clear();

            GetMarketing();
            for (int i = 0; i < namaMarketing.Count; i++)
            {
                GetNamaDanJumlahSekolah(idMarketing[i]);
            }
            DesignGridView();
            GetKatjaw();

            for (int i = 0; i < namaSekolah.Count; i++)
            {
                GetBanyakSiswaPersekolah(namaSekolah[i]);
                GetValuePersesi(namaSekolah[i]);
            }
            //variable untuk increment nilai index list sesi supaya yang terambil index pertama anak dari sekolah selanjutnya
            int a = 0;
            int b = 0;

            int anakKe = 0;


            for (int sekolahKe = 0; sekolahKe < namaSekolah.Count; sekolahKe++)
            {
                b = a;
                while (anakKe < (jumlahAnakPersekolah[sekolahKe] + b))
                {
                    for (int j = 0; j < katjaw.Count; j++)
                    {
                        //Mengisi datagridview sesi 1
                        if (sesi1[anakKe] == katjaw[j] && dataGridView1.Rows[j].Cells[2 + (5 * sekolahKe)].Value == null)
                        {
                            dataGridView1.Rows[j].Cells[2 + (5 * sekolahKe)].Value = 1;
                        }
                        else if (sesi1[anakKe] != katjaw[j] && dataGridView1.Rows[j].Cells[2 + (5 * sekolahKe)].Value == null)
                        {
                            dataGridView1.Rows[j].Cells[2 + (5 * sekolahKe)].Value = 0;
                        }
                        else if (sesi1[anakKe] == katjaw[j] && dataGridView1.Rows[j].Cells[2 + (5 * sekolahKe)].Value != null)
                        {
                            dataGridView1.Rows[j].Cells[2 + (5 * sekolahKe)].Value = int.Parse(dataGridView1.Rows[j].Cells[2 + (5 * sekolahKe)].Value.ToString()) + 1;
                        }

                        //Mengisi datagridview sesi 2
                        if (sesi2[anakKe] == katjaw[j] && dataGridView1.Rows[j].Cells[3 + (5 * sekolahKe)].Value == null)
                        {
                            dataGridView1.Rows[j].Cells[3 + (5 * sekolahKe)].Value = 1;
                        }
                        else if (sesi2[anakKe] != katjaw[j] && dataGridView1.Rows[j].Cells[3 + (5 * sekolahKe)].Value == null)
                        {
                            dataGridView1.Rows[j].Cells[3 + (5 * sekolahKe)].Value = 0;
                        }
                        else if (sesi2[anakKe] == katjaw[j] && dataGridView1.Rows[j].Cells[3 + (5 * sekolahKe)].Value != null)
                        {
                            dataGridView1.Rows[j].Cells[3 + (5 * sekolahKe)].Value = int.Parse(dataGridView1.Rows[j].Cells[3 + (5 * sekolahKe)].Value.ToString()) + 1;
                        }

                        //Mengisi datagridview sesi 3
                        if (sesi3[anakKe] == katjaw[j] && dataGridView1.Rows[j].Cells[4 + (5 * sekolahKe)].Value == null)
                        {
                            dataGridView1.Rows[j].Cells[4 + (5 * sekolahKe)].Value = 1;
                        }
                        else if (sesi3[anakKe] != katjaw[j] && dataGridView1.Rows[j].Cells[4 + (5 * sekolahKe)].Value == null)
                        {
                            dataGridView1.Rows[j].Cells[4 + (5 * sekolahKe)].Value = 0;
                        }
                        else if (sesi3[anakKe] == katjaw[j] && dataGridView1.Rows[j].Cells[4 + (5 * sekolahKe)].Value != null)
                        {
                            dataGridView1.Rows[j].Cells[4 + (5 * sekolahKe)].Value = int.Parse(dataGridView1.Rows[j].Cells[4 + (5 * sekolahKe)].Value.ToString()) + 1;
                        }

                        //Mengisi datagridview sesi 4
                        if (sesi4[anakKe] == katjaw[j] && dataGridView1.Rows[j].Cells[5 + (5 * sekolahKe)].Value == null)
                        {
                            dataGridView1.Rows[j].Cells[5 + (5 * sekolahKe)].Value = 1;
                        }
                        else if (sesi4[anakKe] != katjaw[j] && dataGridView1.Rows[j].Cells[5 + (5 * sekolahKe)].Value == null)
                        {
                            dataGridView1.Rows[j].Cells[5 + (5 * sekolahKe)].Value = 0;
                        }
                        else if (sesi4[anakKe] == katjaw[j] && dataGridView1.Rows[j].Cells[5 + (5 * sekolahKe)].Value != null)
                        {
                            dataGridView1.Rows[j].Cells[5 + (5 * sekolahKe)].Value = int.Parse(dataGridView1.Rows[j].Cells[5 + (5 * sekolahKe)].Value.ToString()) + 1;
                        }

                        //Mengisi datagridview sesi 5
                        if (sesi5[anakKe] == katjaw[j] && dataGridView1.Rows[j].Cells[6 + (5 * sekolahKe)].Value == null)
                        {
                            dataGridView1.Rows[j].Cells[6 + (5 * sekolahKe)].Value = 1;
                        }
                        else if (sesi5[anakKe] != katjaw[j] && dataGridView1.Rows[j].Cells[6 + (5 * sekolahKe)].Value == null)
                        {
                            dataGridView1.Rows[j].Cells[6 + (5 * sekolahKe)].Value = 0;
                        }
                        else if (sesi5[anakKe] == katjaw[j] && dataGridView1.Rows[j].Cells[6 + (5 * sekolahKe)].Value != null)
                        {
                            dataGridView1.Rows[j].Cells[6 + (5 * sekolahKe)].Value = int.Parse(dataGridView1.Rows[j].Cells[6 + (5 * sekolahKe)].Value.ToString()) + 1;
                        }
                    }
                    anakKe++;
                    if (anakKe == ((jumlahAnakPersekolah[sekolahKe] + b) - 1))
                    {
                        a = (anakKe + 1);
                    }
                }
            }

            //inisialisasi hasil tiap tiap katjaw = 0 dan kategori jawaban
            for (int i = 0; i < katjaw.Count; i++)
            {
                //hasil sesi
                hasilSesi1.Add(0);
                hasilSesi2.Add(0);
                hasilSesi3.Add(0);
                hasilSesi4.Add(0);
                hasilSesi5.Add(0);

                //kategori
                katjawSesi1.Add(katjaw[i]);
                katjawSesi2.Add(katjaw[i]);
                katjawSesi3.Add(katjaw[i]);
                katjawSesi4.Add(katjaw[i]);
                katjawSesi5.Add(katjaw[i]);
                katjawTotal.Add(katjaw[i]);
            }

            //ambil tiap tiap nilai per sesi dari tiap sekolah
            for (int sekolahKe = 0; sekolahKe < namaSekolah.Count; sekolahKe++)
            {
                int jumlah1 = 0;
                int jumlah2 = 0;
                int jumlah3 = 0;
                int jumlah4 = 0;
                int jumlah5 = 0;
                for (int i = 0; i < katjaw.Count; i++)
                {
                    //mengisi nilai jumlah untuk tiap tiap sesi dari tiap - tiap sekolah
                    jumlah1 += int.Parse(dataGridView1.Rows[i].Cells[2 + (5 * sekolahKe)].Value.ToString());
                    jumlah2 += int.Parse(dataGridView1.Rows[i].Cells[3 + (5 * sekolahKe)].Value.ToString());
                    jumlah3 += int.Parse(dataGridView1.Rows[i].Cells[4 + (5 * sekolahKe)].Value.ToString());
                    jumlah4 += int.Parse(dataGridView1.Rows[i].Cells[5 + (5 * sekolahKe)].Value.ToString());
                    jumlah5 += int.Parse(dataGridView1.Rows[i].Cells[6 + (5 * sekolahKe)].Value.ToString());

                    //mengisi hasil masing masing sesi terhadap masing masing katjaw
                    hasilSesi1[i] += int.Parse(dataGridView1.Rows[i].Cells[2 + (5 * sekolahKe)].Value.ToString());
                    hasilSesi2[i] += int.Parse(dataGridView1.Rows[i].Cells[3 + (5 * sekolahKe)].Value.ToString());
                    hasilSesi3[i] += int.Parse(dataGridView1.Rows[i].Cells[4 + (5 * sekolahKe)].Value.ToString());
                    hasilSesi4[i] += int.Parse(dataGridView1.Rows[i].Cells[5 + (5 * sekolahKe)].Value.ToString());
                    hasilSesi5[i] += int.Parse(dataGridView1.Rows[i].Cells[6 + (5 * sekolahKe)].Value.ToString());
                }
                sumSesi1.Add(jumlah1);
                sumSesi2.Add(jumlah2);
                sumSesi3.Add(jumlah3);
                sumSesi4.Add(jumlah4);
                sumSesi5.Add(jumlah5);
            }

            //hapus katjaw dan nilai yang kosong buat charting
            KatjawForCharting(katjawSesi1, hasilSesi1);
            KatjawForCharting(katjawSesi2, hasilSesi2);
            KatjawForCharting(katjawSesi3, hasilSesi3);
            KatjawForCharting(katjawSesi4, hasilSesi4);
            KatjawForCharting(katjawSesi5, hasilSesi5);

            dataGridView1.Rows.Add("", "Total");

            for (int i = 0; i < namaSekolah.Count; i++)
            {
                dataGridView1.Rows[dataGridView1.RowCount - 1].Cells[2 + (5 * i)].Value = sumSesi1[i];
                dataGridView1.Rows[dataGridView1.RowCount - 1].Cells[3 + (5 * i)].Value = sumSesi2[i];
                dataGridView1.Rows[dataGridView1.RowCount - 1].Cells[4 + (5 * i)].Value = sumSesi3[i];
                dataGridView1.Rows[dataGridView1.RowCount - 1].Cells[5 + (5 * i)].Value = sumSesi4[i];
                dataGridView1.Rows[dataGridView1.RowCount - 1].Cells[6 + (5 * i)].Value = sumSesi5[i];
            }

            //ambil tiap tiap nilai per kategori dari tiap sekolah dan sesi
            for (int i = 0; i < katjaw.Count; i++)
            {
                int jumlah = 0;
                for (int j = 0; j < dataGridView1.ColumnCount - 2; j++)
                {
                    jumlah += int.Parse(dataGridView1.Rows[i].Cells[2 + j].Value.ToString());
                }
                sumPerkatjaw.Add(jumlah);
            }

            dataGridView1.Columns.Add("totalangka", "Total");
            dataGridView1.Columns.Add("totalpersen", "Persentase");
            dataGridView1.Columns["totalangka"].Width = 50;
            dataGridView1.Columns["totalpersen"].Width = 65;
            dataGridView1.Columns["totalpersen"].DefaultCellStyle.Format = "P0";
            dataGridView1.Columns["totalpersen"].DefaultCellStyle.Format = "P2";

            for (int i = 0; i < katjaw.Count; i++)
            {
                dataGridView1.Rows[i].Cells[dataGridView1.ColumnCount - 2].Value = sumPerkatjaw[i];
            }

            sumAll = sumSesi1.Sum() + sumSesi2.Sum() + sumSesi3.Sum() + sumSesi4.Sum() + sumSesi5.Sum();

            dataGridView1.Rows[dataGridView1.RowCount - 1].Cells[dataGridView1.ColumnCount - 2].Value = sumAll;

            for (int i = 0; i < katjaw.Count; i++)
            {
                dataGridView1.Rows[i].Cells[dataGridView1.ColumnCount - 1].Value = sumPerkatjaw[i] / sumAll;
            }

            dataGridView1.Rows[dataGridView1.RowCount - 1].Cells[dataGridView1.ColumnCount - 1].Value = sumAll / sumAll;

            //hapus kategori yang nilainya 0 untuk sesi total
            KatjawForCharting(katjawTotal, sumPerkatjaw);
        }

        private void Cmb_TahunPromo_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowData();
            Btn_Chart.Enabled = true;
            Btn_Export.Enabled = true;
        }

        private void Form_Report_Pertahun_Promosi_FormClosing(object sender, FormClosingEventArgs e)
        {
            Main.isChildFormAppeared = !Main.isChildFormAppeared;
        }

        private void btn_exportToexcel()
        {
            Save_Dialog();
        }

        private void MergeExceldanDiagram()
        {
            //Design Excel
            object missValue = System.Reflection.Missing.Value;
            xlApp = new Excel.Application();
            xlWb = xlApp.Workbooks.Add(missValue);
            xlWs = (Excel.Worksheet)xlWb.Worksheets.get_Item(1);
            xlWs.Name = "Rekap Data";
            
            //Mengenerate cells menjadi otomatis nama sekolah
            for (int i = 0; i < namaSekolah.Count; i++)
            {
                batasAbjad1 = abjad[(i * 5) + 2];
                batasAbjad2 = abjad[(i * 5) + 6];
                string final = batasAbjad1 + "7 : " + batasAbjad2 + "7";
                xlWs.get_Range(final).Merge();
                // desain sel header untuk nama sekolah
                xlWs.get_Range(final).Cells.Borders.Weight = Excel.XlBorderWeight.xlMedium;
                xlWs.get_Range(final).Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                xlWs.get_Range(final).Cells.HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;
                //isi header nama sekolah
                xlWs.get_Range(final).Formula = namaSekolah[i];
                // desain sel header untuk untuk sesi ke -
                string final2 = batasAbjad1 + "8 : " + batasAbjad2 + "8";
                xlWs.get_Range(final2).Cells.Borders.Weight = Excel.XlBorderWeight.xlMedium;
                xlWs.get_Range(final2).Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                xlWs.get_Range(final2).Cells.HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;
                // isi header cell sesi ke -
                string hurufBatasAbjad1 = abjad[(i * 5) + 2];
                string batas_sesi1 = hurufBatasAbjad1 + "8";
                xlWs.get_Range(batas_sesi1).Formula = "1";
                string hurufBatasAbjad2 = abjad[(i * 5) + 3];
                string batas_sesi2 = hurufBatasAbjad2 + "8";
                xlWs.get_Range(batas_sesi2).Formula = "2";
                string hurufBatasAbjad3 = abjad[(i * 5) + 4];
                string batas_sesi3 = hurufBatasAbjad3 + "8";
                xlWs.get_Range(batas_sesi3).Formula = "3";
                string hurufBatasAbjad4 = abjad[(i * 5) + 5];
                string batas_sesi4 = hurufBatasAbjad4 + "8";
                xlWs.get_Range(batas_sesi4).Formula = "4";
                string hurufBatasAbjad5 = abjad[(i * 5) + 6];
                string batas_sesi5 = hurufBatasAbjad5 + "8";
                xlWs.get_Range(batas_sesi5).Formula = "5";

                //Bikin border buat isi
                string final3 = batasAbjad1 + "9 : " + batasAbjad2 + "40";
                xlWs.get_Range(final3).Cells.Borders.Weight = Excel.XlBorderWeight.xlThin;
                xlWs.get_Range(final3).Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                xlWs.get_Range(final3).Cells.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
            }

            string abjad_awal;
            int indexAbjadAwal = 0;
            int rentang;
            int indexAbjadAkhir;
            string abjad_akhir;

            for (int i = 0; i < idMarketing2.Count; i++)
            {
                queryUntukNamaMarketing(Cmb_TahunPromo.Text);
                queryUntukJumlahSekolah(Cmb_TahunPromo.Text, idMarketing2[i]);
                // update nilai indexabjadawal
                if (i == 0)
                    indexAbjadAwal = 2; // C

                // penentuan abjad awal
                abjad_awal = abjad[indexAbjadAwal];
                
                // penentuan rentang
                rentang = (banyakSekolahPermarketing2.Count * 5) - 1;
                
                // penentuan index abjad akhir
                indexAbjadAkhir = indexAbjadAwal + rentang;
                
                // penentuan abjad akhir
                abjad_akhir = abjad[indexAbjadAkhir];
               
                // update nilai index awal untuk i selanjutnya
                indexAbjadAwal = indexAbjadAkhir + 1;

                // range
                string range = abjad_awal + "6 : " + abjad_akhir + "6";
                xlWs.get_Range(range).Merge();
                xlWs.get_Range(range).Formula = namaMarketing2[i];
                xlWs.get_Range(range).Cells.Borders.Weight = Excel.XlBorderWeight.xlMedium;
                xlWs.get_Range(range).Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                xlWs.get_Range(range).Cells.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;

            }
    
            // Desain untuk cell total
            HurufBatasTotal = abjad[(namaSekolah.Count) * 5 + 2];
            
            string batas_Total = HurufBatasTotal + "6 : " + HurufBatasTotal + "8";
            xlWs.get_Range(batas_Total).Merge();
            xlWs.get_Range(batas_Total).Cells.Borders.Weight = Excel.XlBorderWeight.xlMedium;
            xlWs.get_Range(batas_Total).Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            xlWs.get_Range(batas_Total).Cells.HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;
            xlWs.get_Range(batas_Total).Formula = "Total";
            xlWs.get_Range(batas_Total).ColumnWidth = 10;
            xlWs.get_Range(batas_Total).Cells.Font.Bold = true;

            // Desain untuk cell Persentase
            HurufBatasPersen = abjad[(namaSekolah.Count) * 5 + 3];
            string bts_Persen = HurufBatasPersen + "6 : " + HurufBatasPersen + "8";
            xlWs.get_Range(bts_Persen).Merge();
            xlWs.get_Range(bts_Persen).Cells.Borders.Weight = Excel.XlBorderWeight.xlMedium;
            xlWs.get_Range(bts_Persen).Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            xlWs.get_Range(bts_Persen).Cells.HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;
            xlWs.get_Range(bts_Persen).Formula = "Persentase";
            xlWs.get_Range(bts_Persen).ColumnWidth = 15;
            xlWs.get_Range(bts_Persen).Cells.Font.Bold = true; ;
            float x = float.Parse("0.00");
            float hasil = x * 1000;
            string hsl = hasil.ToString();
            xlWs.get_Range(HurufBatasPersen + "9 : " + HurufBatasPersen + "40").NumberFormat = hsl + "%";

            // tambahan isi
            string Total = HurufBatasTotal + "9 : " + HurufBatasPersen + "40";
            xlWs.get_Range(Total).Cells.Borders.Weight = Excel.XlBorderWeight.xlThin;
            xlWs.get_Range(Total).Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            xlWs.get_Range(Total).Cells.HorizontalAlignment = Excel.XlVAlign.xlVAlignCenter;
            
            // Merge cells pasti
            //xlWs.get_Range("A6 : A8 ; B6 : B8").Merge();
            xlWs.Range[xlWs.Cells[6, 1], xlWs.Cells[8, 1]].Merge();
            xlWs.Range[xlWs.Cells[6, 2], xlWs.Cells[8, 2]].Merge();
            
            // alignment
            xlWs.get_Range("A6 : A8").Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            xlWs.get_Range("B6 : B8").Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            xlWs.get_Range("A6 : A40").Cells.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
            xlWs.get_Range("B6 : B8").Cells.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
            
            //memberi border
            xlWs.get_Range("A6 : A8").Cells.Borders.Weight = Excel.XlBorderWeight.xlMedium;
            xlWs.get_Range("B6 : B8").Cells.Borders.Weight = Excel.XlBorderWeight.xlMedium;
            xlWs.get_Range("A9 : B40").Cells.Borders.Weight = Excel.XlBorderWeight.xlThin;
            
            //width
            xlWs.get_Range("B6").ColumnWidth = 55;
            
            // memberi nama header pasti tetap
            xlWs.get_Range("A6").Formula = "No";
            xlWs.get_Range("B6").Formula = "Nama Kategori";
            
            // memberi Judul
            string batasMerge = "A1 : " + HurufBatasPersen + "1";
            xlWs.get_Range(batasMerge).Merge();
            xlWs.get_Range(batasMerge).Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            xlWs.get_Range(batasMerge).Cells.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
            xlWs.get_Range(batasMerge).Formula = "Rekap Data Sesi Peneleponan Per Tahun Promosi  " + Cmb_TahunPromo.Text;

        }

        private void queryUntukNamaMarketing(string tahun_promosi)
        {
            query = "SELECT DISTINCT nama_marketing FROM tb_sesipeneleponan INNER JOIN tb_marketing ON tb_sesipeneleponan.id_marketing = tb_marketing.id_marketing WHERE tahun_promosi ='" + tahun_promosi + "'";
            cmd = new MySqlCommand(query, conn);
            try
            {
                conn.Open();
                namaMarketing2.Clear();
                DataTable dt = new DataTable();
                adpt = new MySqlDataAdapter(cmd);
                adpt.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    namaMarketing2.Add(r[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void queryUntukIdMarketing(string tahun_promosi)
        {
            query = "SELECT DISTINCT id_marketing FROM tb_sesipeneleponan WHERE tahun_promosi='" + tahun_promosi + "'";
            cmd = new MySqlCommand(query, conn);
            try
            {
                conn.Open();
                idMarketing2.Clear();
                DataTable dt = new DataTable();
                adpt = new MySqlDataAdapter(cmd);
                adpt.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    idMarketing2.Add(r[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void queryUntukJumlahSekolah(string tahun_promo, string id_marketing)
        {
            query = "SELECT DISTINCT asal_sekolah FROM tb_sesipeneleponan INNER JOIN tb_marketing ON tb_sesipeneleponan.id_marketing = tb_marketing.id_marketing WHERE tahun_promosi ='" + tahun_promo + "'AND tb_sesipeneleponan.id_marketing='" + id_marketing + "'";
            cmd = new MySqlCommand(query, conn);
            try
            {
                conn.Open();

                buatBanyakSekolah2.Clear();
                banyakSekolahPermarketing2.Clear();
                DataTable dt = new DataTable();
                adpt = new MySqlDataAdapter(cmd);
                adpt.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    buatBanyakSekolah2.Add(r[0].ToString());
                    banyakSekolahPermarketing2.Add(buatBanyakSekolah2.Count);
                }

                conn.Close();
            }
                
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void PengsianCell(DataGridView dg2)
        {
            int i;
            int j;

            xlWs = xlWs = (Excel.Worksheet)xlWb.Worksheets.get_Item(1);
            try
            {
                for (i = 0; i < dg2.Rows.Count; i++)
                {
                    for (j = 0; j < dg2.Columns.Count; j++)
                    {
                        if (i <= dg2.Rows.Count && dg2.Rows[i].Cells[j].Value != null)
                        {
                            if (j < dg2.Columns.Count && dg2.Rows[i].Cells[j].Value != null)
                            {
                                if (j > 1)
                                {
                                    xlWs.Cells[i + 9, j + 1] = float.Parse(dg2.Rows[i].Cells[j].Value.ToString());
                                }
                                else
                                {
                                    xlWs.Cells[i + 9, j + 1] = dg2.Rows[i].Cells[j].Value.ToString();
                                }
                            }

                        }
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void ChartSheets()
        {
            for (int g = 0; g < 2; g++)
            {
                // Pengaturan sheet
                xlWs = (Excel.Worksheet)xlWb.Worksheets.get_Item(2);
                xlWs.Name = "Diagrams";
                xlWs.Tab.Color = Color.Orange;

                xlWs.get_Range("A1 : N1").Merge();
                xlWs.get_Range("A1 : N1").Font.Size = 14;
                xlWs.get_Range("A1 : N1").Font.Bold = true;
                xlWs.get_Range("A1 : N1").Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                xlWs.get_Range("A1 : N1").Cells.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                xlWs.Cells[1, 1] = "Kumpulan Grafik Data Sesi Peneleponan Tahun Promosi Seluruh Marketing" + Cmb_TahunPromo.Text ;

                if (g == 0)
                {
                    //// chart Batang
                    //var charts = xlWs.ChartObjects() as Microsoft.Office.Interop.Excel.ChartObjects;
                    //var chartObject = charts.Add(60, 80, 700, 700) as Microsoft.Office.Interop.Excel.ChartObject;
                    //var chart = chartObject.Chart;
                    //xlWs = (Excel.Worksheet)xlWb.Worksheets.get_Item(1);
                    //var range = xlWs.get_Range("B6 : B38 ; " + HurufBatasTotal + "6 : " + HurufBatasTotal + "38");
                    //chart.SetSourceData(range);
                    //chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered;
                    //chart.ChartWizard(Source: range,
                    //    Title: "Statistik Data Sesi Peneleponan Seluruh Marketing Tahun Promosi" + Cmb_TahunPromo.Text,
                    //    CategoryTitle: "Hasil Peneleponan",
                    //    ValueTitle: "Jumlah Data");

                    MakingChart(60, 80, 700, 700, HurufBatasTotal + "6", HurufBatasTotal + "38",
                        "B6", "B38", "Statistik Data Sesi Peneleponan Seluruh Marketing Tahun Promosi " + Cmb_TahunPromo.Text, Excel.XlChartType.xlColumnClustered);
                }
                if (g == 1)
                {
                    //var charts = xlWs.ChartObjects() as Microsoft.Office.Interop.Excel.ChartObjects;
                    //var chartObject = charts.Add(60, 900, 750, 750) as Microsoft.Office.Interop.Excel.ChartObject;
                    //var chart = chartObject.Chart;
                    //xlWs = (Excel.Worksheet)xlWb.Worksheets.get_Item(1);
                    //var range = xlWs.get_Range("B9 : B39 ; " + HurufBatasPersen + "9 : " + HurufBatasPersen + "39");
                    //chart.SetSourceData(range);
                    //chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlPie;
                    //chart.HasTitle = true;
                    //chart.ChartTitle.Text = "Persentase peneleponan";

                    MakingChart(60, 900, 700, 700, HurufBatasPersen + "9", HurufBatasPersen + "39",
                        "B9", "B39", "Persentase Data Sesi Peneleponan Seluruh Marketing Tahun Promosi " + Cmb_TahunPromo.Text, 
                        Excel.XlChartType.xlPie);
                }
            }

        }

        private void MakingChart(double x, double y, double w, double h, string nilaiR1, string nilaiR2,
            string catR1, string catR2, string textTitle, Excel.XlChartType type)
        {
            object missValue = System.Reflection.Missing.Value;

            Excel.ChartObjects charts = (Excel.ChartObjects)xlWs.ChartObjects(missValue);
            Excel.ChartObject chartObject = charts.Add(x, y, w, h) as Excel.ChartObject;
            Excel.Chart chart = chartObject.Chart;
            xlWs = (Excel.Worksheet)xlWb.Worksheets.get_Item(1);

            // range untuk nilai - input range
            var range = xlWs.get_Range(nilaiR1 + ":" + nilaiR2);

            // input range ke source data
            chart.SetSourceData(range);

            // chart type
            chart.ChartType = type;

            // x categories
            Excel.Axis axis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlCategory, Excel.XlAxisGroup.xlPrimary);
            Excel.Range rangeAxis = xlWs.get_Range(catR1, catR2);

            // implement axis
            axis.CategoryNames = rangeAxis;

            // chart title
            chart.HasTitle = true;
            chart.ChartTitle.Text = textTitle;
            chart.ApplyDataLabels(Excel.XlDataLabelsType.xlDataLabelsShowPercent);
        }

        private void Save_Dialog()
        {
            Sfd_ExportPersekolah.InitialDirectory = "D:";
            Sfd_ExportPersekolah.Title = "Save as Excel File";
            Sfd_ExportPersekolah.FileName = "Rekap Data Sesi Telepon Per Tahun Promo " + Cmb_TahunPromo.Text;
            Sfd_ExportPersekolah.Filter = "Excel Files(2003)|*.xls|Excel Files(2010)|*.xlsx";
            if (Sfd_ExportPersekolah.ShowDialog() != DialogResult.Cancel)
            {
                //try
                //{
                    queryUntukIdMarketing(Cmb_TahunPromo.Text);
                    MergeExceldanDiagram();
                    PengsianCell(dataGridView1);
                    ChartSheets();
                    
                //}
                //catch (Exception ex)
                //{
                //    MessageBox.Show(ex.Message);
                //}
                xlApp.ActiveWorkbook.SaveCopyAs(Sfd_ExportPersekolah.FileName.ToString());
                xlApp.ActiveWorkbook.Saved = true;
                xlApp.Quit();

                releaseObject(xlWs);
                releaseObject(xlWb);
                releaseObject(xlApp);

                MessageBox.Show("Excel file created");
            }
            else
            {

            }
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        private void Btn_Export_Click(object sender, EventArgs e)
        {
            btn_exportToexcel();
        }

        public static bool isChartAppeared = false;
        private void Btn_Chart_Click(object sender, EventArgs e)
        {
            if(!isChartAppeared)
            {
                isChartAppeared = !isChartAppeared;

                Form_Chart_Report_Pertahun_Promosi formChart = new Form_Chart_Report_Pertahun_Promosi();
                formChart.MdiParent = this.MdiParent;
                formChart.Show();
            }
        }
    }
}
