﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using MySql.Data.MySqlClient;

namespace Admin_Tele
{
    public partial class SmsGatewaySiswa : Form
    {
        MySqlConnection connection;
        MySqlCommand cmd;
        MySqlDataAdapter adp;

        string query;
        string pathConnection;
        string sheetExcel = "Sheet1";
        string pathFile;

        
        bool radioBtnCekTidakTerkirim = false;
        //bool udahAda;

        public SmsGatewaySiswa()
        {
            InitializeComponent();
            gBox_SentItems.Enabled = false;
            rdBtn_Normal.Checked = false;
            rdBtn_TidakTerkirim.Checked = false;
            btn_KirimSms.Enabled = false;
        }

        private void importExcel(string path)
        {
            try
            {
                //dgv_DataSiswa.ColumnCount = 1;
                //dgv_DataSiswa.Columns[0].Name = "hp";

                dgv_DataSiswa.Columns.Clear();
                dgv_DataSiswa.Rows.Clear();

                // Untuk Microsoft Office 2010
                pathConnection = "Provider = Microsoft.ACE.OLEDB.12.0; Data Source=" + path + ";Extended Properties=\"Excel 12.0; HDR=Yes;\";";
                OleDbConnection conn = new OleDbConnection(pathConnection);

                OleDbDataAdapter dataAdapter = new OleDbDataAdapter("Select * FROM [" + sheetExcel + "$]", pathConnection);

                DataTable dt = new DataTable();
                dataAdapter.Fill(dt);
                dgv_DataSiswa.DataSource = dt;

                //MessageBox.Show("ukuran: " + dt.Rows.Count);

                foreach (DataRow r in dt.Rows)
                {
                    NoHpdariexcel.Add(r[3].ToString());
                    //dgv_DataSiswa.Rows.Add(r[0].ToString());
                }

                
                btn_KirimSms.Enabled = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            #region excel dengan tombol udah di coment
            //OpenFileDialog open = new OpenFileDialog();

            //if (open.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            //{
            //    //path = open.FileName;
            //    // Untuk Microsoft Office 2010
            //    pathConnection = "Provider = Microsoft.ACE.OLEDB.12.0; Data Source=" + path + ";Extended Properties=\"Excel 12.0; HDR=Yes;\";";
            //    OleDbConnection conn = new OleDbConnection(pathConnection);

            //    OleDbDataAdapter dataAdapter = new OleDbDataAdapter("Select * FROM [" + cbox + "$]", pathConnection);

            //    DataTable dt = new DataTable();
            //    dataAdapter.Fill(dt);
            //    dgv_tabelPenerimaan.DataSource = dt;

            //    DesainGridView();
            //    btn_SendSMS.Enabled = true;
            //    TeksBoxClear();
            //} 
            #endregion
        }

        void FilterExcelKeGrid()
        {
            //for (int i = NoHpdariexcel.Count - 1; i >= 0; i--)
            //{
            //    if (dgv_DataSiswa.Rows[i].Cells[3].Value == NoHpdariexcel[i])
            //    {
            //        MessageBox.Show("NohpDari Excel : " + NoHpdariexcel[i]);
            //        MessageBox.Show("Nodidgv : " + dgv_DataSiswa.Rows[i].Cells[3].Value.ToString());
            //        //dgv_DataSiswa.Rows.Remove(dgv_DataSiswa.Rows[i]);
            //    }
            //}
        }

        List<string> NoHpdariexcel = new List<string>();

        //void AmbilNohpdariExcel(string path)
        //{
        //    pathConnection = "Provider = Microsoft.ACE.OLEDB.12.0; Data Source=" + path + ";Extended Properties=\"Excel 12.0; HDR=Yes;\";";
        //    OleDbConnection conn = new OleDbConnection(pathConnection);

        //    OleDbDataAdapter dataAdapter = new OleDbDataAdapter("Select NoHP FROM [" + sheetExcel + "$]", pathConnection);

        //    DataTable dt = new DataTable();
        //    dataAdapter.Fill(dt);
        //    //dgv_DataSiswa.DataSource = dt;
        //    foreach (DataRow r in dt.Rows)
        //    {
        //        NoHpdariexcel.Add(r[0].ToString());
        //    }
                
        //}

        //List<string> noHpYgAdaDidgv = new List<string>();

        private void btn_ImportFile_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog open = new OpenFileDialog();

                if (open.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    pathFile = open.FileName;

                    if (nosiswaFromtb.Count > 0)
                    {
                        for (int i = 0; i < nosiswaFromtb.Count; i++)
                        {
                            for (int j = 0; j < NoHpdariexcel.Count; j++)
                            {
                                if (nosiswaFromtb[i] == NoHpdariexcel[j])
                                {
                                    //FilterExcelKeGrid();
                                    MessageBox.Show("hai" );
                                }
                            }
                        }
                    }
                    else
                    {
                        importExcel(pathFile);
                        desainGrid();

                        for (int i = 0; i < dgv_DataSiswa.Rows.Count; i++)
                        {
                            dgv_DataSiswa.Rows[i].Cells[4].Value = false;
                        }

                    }                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void desainGrid()
        {
            
            DataGridViewCheckBoxColumn CheckboxColumn = new DataGridViewCheckBoxColumn();
            CheckboxColumn.HeaderText = "Check Validasi";
            dgv_DataSiswa.Columns.Add(CheckboxColumn);
            dgv_DataSiswa.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgv_DataSiswa.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            //udahAda = true;
        }

        List<string> noHpYgDicek = new List<string>();

        void cekDiDgv()
        {
            noHpYgDicek.Clear();
            terpilih.Clear();

            for (int i = 0; i < dgv_DataSiswa.Rows.Count; i++)
            {
                bool isChecked = (bool)dgv_DataSiswa.Rows[i].Cells[4].Value;   

                if (isChecked == true)
                {
                    //MessageBox.Show("Ini di cek");
                    noHpYgDicek.Add(dgv_DataSiswa.Rows[i].Cells[3].Value.ToString());
                    terpilih.Add(dgv_DataSiswa.Rows[i].Index);
                }
                else
                {
                    //MessageBox.Show("Ini tidak dicek");
                }
            }
        }

        private void btn_KirimSms_Click(object sender, EventArgs e)
        {
            //
            cekDiDgv();
            
            // kirim ke gamu
            //cekDgvdanSentItems();
            isiBtnKirimSMS(noHpYgDicek, nohpYangAdadidb);

            //Kirim ke tb sms siswa
            InsertTotbsms(noHpYgDicek);

            // refresh nomor hape yg telah terkirim (nomor yang telah dikirim dihapus dari grid)
            SetUlangGrid(terpilih);
        }

        List<string> noUntukdisms = new List<string>();
        
        void SetUlangGrid(List<int> hapusIndex)
        {
            try
            {
                for (int i = terpilih.Count - 1; i >= 0 ; i--)
                {
                    dgv_DataSiswa.Rows.RemoveAt(terpilih[i]);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            //udahAda = false;
        }

        void isiBtnKirimSMS(List<string> listNoHape, List<string> listTidakTerkirim)
     {
            connection = new MySqlConnection(Constanta.conString);
            if (rchBox_TextSms.Text != "")
            {
                if (rdBtn_Normal.Checked == true)
                {
                    for (int i = 0; i < listNoHape.Count; i++)
                    {
                        query = "INSERT INTO outbox (DestinationNumber, TextDecoded, DeliveryReport) VALUES (@dn, @td, 'no')";
                        cmd = new MySqlCommand(query, connection);
                        cmd.Parameters.AddWithValue("@dn", listNoHape[i]);
                        cmd.Parameters.AddWithValue("@td", rchBox_TextSms.Text);
                        try
                        {
                            connection.Open();
                            if (cmd.ExecuteNonQuery() > 0)
                            {
                                if (i == listNoHape.Count - 1)
                                {
                                    MessageBox.Show("SMS has been inputed");
                                }
                            }
                            connection.Close();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                }
                else if (rdBtn_TidakTerkirim.Checked == true)
                {
                    for (int i = 0; i < listTidakTerkirim.Count; i++)
                    {
                        query = "INSERT INTO outbox (DestinationNumber, TextDecoded, DeliveryReport) VALUES (@dn, @td, 'no')";
                        cmd = new MySqlCommand(query, connection);
                        cmd.Parameters.AddWithValue("@dn", listTidakTerkirim[i]);
                        cmd.Parameters.AddWithValue("@td", rchBox_TextSms.Text);
                        try
                        {
                            connection.Open();
                            if (cmd.ExecuteNonQuery() > 0)
                            {
                                if (i == listTidakTerkirim.Count - 1)
                                {
                                    MessageBox.Show("SMS has been inputed");
                                }
                            }
                            connection.Close();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }

                }
            }
            
        }

        List<string> nohpYangAdadidb = new List<string>();

        void GetDataTidakTerkirim()
        {
            //MessageBox.Show("a");
            lBox_SentItems.Items.Clear();
            nohpYangAdadidb.Clear();
            //MessageBox.Show("test");
            
            connection = new MySqlConnection(Constanta.conString);

            //query = "SELECT DISTINCT DestinationNumber FROM sentitems WHERE DestinationNumber ='" + noHpYgAdaDidgv[i] + "' AND Status = 'SendingError'";
            query = "SELECT a.DestinationNumber FROM (SELECT DestinationNumber, COUNT(*) AS banyakerror FROM sentitems WHERE Status = 'SendingError' GROUP BY DestinationNumber) a INNER JOIN (SELECT DestinationNumber, Status, COUNT(*) AS banyakdata FROM sentitems GROUP BY DestinationNumber HAVING COUNT(*) > 1 OR Status = 'SendingError') b ON a.DestinationNumber = b.DestinationNumber WHERE a.banyakerror = b.banyakdata";
            cmd = new MySqlCommand(query, connection);
            try
            {
                adp = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);

                foreach (DataRow r in dt.Rows)
                {
                    if (r[0].ToString() != "")
                    {
                        lBox_SentItems.Items.Add(r[0].ToString());
                        nohpYangAdadidb.Add(r[0].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            //MessageBox.Show(nohpYangTidakTerkirim[i].ToString());
        }

        //ambil data dari tb siswa
        List<string> nosiswaFromtb = new List<string>();
        void getNomerHpdarismsSiswa()
        {
            nosiswaFromtb.Clear();

            connection = new MySqlConnection(Constanta.conString);
            query = "SELECT DestinationNumber FROM tb_smssiswasma";
            cmd = new MySqlCommand(query, connection);

            try
            {
                connection.Open();

                adp = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);

                foreach (DataRow r in dt.Rows)
                {
                    nosiswaFromtb.Add(r[0].ToString());
                    //MessageBox.Show(r[0].ToString());
                }

                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //ambil data dari sentitems
        List<string> nosiswaFromsentItems = new List<string>();
        List<string> status = new List<string>();

        void getNosiswadariSentItems()
        {
            nosiswaFromsentItems.Clear();
            status.Clear();

            connection = new MySqlConnection(Constanta.conString);
            query = "SELECT DestinationNumber, Status FROM sentitems";
            cmd = new MySqlCommand(query, connection);

            try
            {
                connection.Open();
                adp = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);

                foreach (DataRow r in dt.Rows)
                {
                    nosiswaFromsentItems.Add(r[0].ToString());
                    status.Add(r[1].ToString());
                    //MessageBox.Show(r[0].ToString());
                }

                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        
        private void btn_GetData_Click(object sender, EventArgs e)
        {
            GetDataTidakTerkirim();
        }

        void PengecekanNohp()
        {
            //MessageBox.Show("hai");
            getNomerHpdarismsSiswa();
            getNosiswadariSentItems();

            for (int i = 0; i < nosiswaFromtb.Count; i++)
            {
                //MessageBox.Show("a : " + nosiswaFromtb[i] );
                for (int j = 0; j < nosiswaFromsentItems.Count; j++)
                {
                    //MessageBox.Show("b : " + nosiswaFromsentItems[j]);
                    if (nosiswaFromtb[i] == nosiswaFromsentItems[j])
                    {
                        //MessageBox.Show("c: sama");
                        UpdateStatus(status[j], nosiswaFromsentItems[j]);
                        break;
                    }
                }
            }
        }

        void UpdateStatus(string stat, string nohp)
        {
            connection = new MySqlConnection(Constanta.conString);
            query = "UPDATE tb_smssiswasma SET status ='" + stat + "' WHERE DestinationNumber ='" + nohp + "'";
            cmd = new MySqlCommand(query, connection);

            try
            {
                connection.Open();

                adp = new MySqlDataAdapter(cmd);
                adp.UpdateCommand = connection.CreateCommand();
                adp.UpdateCommand.CommandText = query;

                if (cmd.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("Data has been update");
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void rdBtn_TidakTerkirim_CheckedChanged(object sender, EventArgs e)
        {
            //radioBtnCekTidakTerkirim = true;
            radioBtnCekTidakTerkirim = !radioBtnCekTidakTerkirim;
            if (radioBtnCekTidakTerkirim)
            {
                gBox_PilihSiswa.Enabled = false;
                gBox_SentItems.Enabled = true;
                btn_KirimSms.Enabled = true;
                //MessageBox.Show("hai");
            }
            else
            {
                gBox_SentItems.Enabled = false;
                gBox_PilihSiswa.Enabled = true;
                btn_KirimSms.Enabled = false;
            }
            
        }

        private void dgv_DataSiswa_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //MessageBox.Show(dgv_DataSiswa.SelectedRows[0].Cells[1].Value.ToString());
            txtBox_Nama.Clear();
            txtBox_NoHp.Clear();
            txtBox_AsalSekolah.Clear();

            txtBox_Nama.Text = dgv_DataSiswa.SelectedRows[0].Cells[1].Value.ToString();
            txtBox_AsalSekolah.Text = dgv_DataSiswa.SelectedRows[0].Cells[2].Value.ToString();
            txtBox_NoHp.Text = dgv_DataSiswa.SelectedRows[0].Cells[3].Value.ToString();

        }

        private void SmsGatewaySiswa_FormClosing(object sender, FormClosingEventArgs e)
        {
            Main.isChildFormAppeared = !Main.isChildFormAppeared;
        }

        private void rdBtn_Normal_CheckedChanged(object sender, EventArgs e)
        {
            btn_KirimSms.Enabled = true;
        }

        List<int> terpilih = new List<int>();

        void InsertTotbsms(List<string> nohpdicek)
        {
            connection = new MySqlConnection(Constanta.conString);
            for (int i = 0; i < nohpdicek.Count; i++)
            {
                query = "INSERT INTO tb_smssiswasma (DestinationNumber) VALUES (@dn)";
                cmd = new MySqlCommand(query, connection);

                cmd.Parameters.AddWithValue("@dn", nohpdicek[i]);
                try
                {
                    connection.Open();
                    if (cmd.ExecuteNonQuery() > 0)
                    {
                        MessageBox.Show("Destination Number has been inputed");
                    }
                    connection.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                } 
            }
        }

        List<string> noSiswa = new List<string>();

        private void btn_refresh_Click(object sender, EventArgs e)
        {
            PengecekanNohp();
        }

    }
}