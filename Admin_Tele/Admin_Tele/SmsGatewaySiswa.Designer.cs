﻿namespace Admin_Tele
{
    partial class SmsGatewaySiswa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.gBox_PilihSiswa = new System.Windows.Forms.GroupBox();
            this.dgv_DataSiswa = new System.Windows.Forms.DataGridView();
            this.txtBox_NoHp = new System.Windows.Forms.TextBox();
            this.txtBox_AsalSekolah = new System.Windows.Forms.TextBox();
            this.txtBox_Nama = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_ImportFile = new System.Windows.Forms.Button();
            this.gBox_SMS = new System.Windows.Forms.GroupBox();
            this.rchBox_TextSms = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_KirimSms = new System.Windows.Forms.Button();
            this.rdBtn_TidakTerkirim = new System.Windows.Forms.RadioButton();
            this.rdBtn_Normal = new System.Windows.Forms.RadioButton();
            this.gBox_SentItems = new System.Windows.Forms.GroupBox();
            this.btn_GetData = new System.Windows.Forms.Button();
            this.lBox_SentItems = new System.Windows.Forms.ListBox();
            this.btn_refresh = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.gBox_PilihSiswa.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_DataSiswa)).BeginInit();
            this.gBox_SMS.SuspendLayout();
            this.gBox_SentItems.SuspendLayout();
            this.SuspendLayout();
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(97, 59);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(448, 25);
            this.label15.TabIndex = 15;
            this.label15.Text = "BAGIAN MARKETING POLITEKNIK PRAKTISI";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(96, 26);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(500, 29);
            this.label14.TabIndex = 14;
            this.label14.Text = "FORM PEMBERITAHUAN SISWA SEKOLAH ";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Admin_Tele.Properties.Resources._6f01f191_595f_4d36_aa53_29c89ac8fb3d;
            this.pictureBox1.Location = new System.Drawing.Point(13, 23);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(78, 69);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 16;
            this.pictureBox1.TabStop = false;
            // 
            // gBox_PilihSiswa
            // 
            this.gBox_PilihSiswa.Controls.Add(this.dgv_DataSiswa);
            this.gBox_PilihSiswa.Controls.Add(this.txtBox_NoHp);
            this.gBox_PilihSiswa.Controls.Add(this.txtBox_AsalSekolah);
            this.gBox_PilihSiswa.Controls.Add(this.txtBox_Nama);
            this.gBox_PilihSiswa.Controls.Add(this.label3);
            this.gBox_PilihSiswa.Controls.Add(this.label2);
            this.gBox_PilihSiswa.Controls.Add(this.label1);
            this.gBox_PilihSiswa.Controls.Add(this.btn_ImportFile);
            this.gBox_PilihSiswa.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gBox_PilihSiswa.Location = new System.Drawing.Point(13, 98);
            this.gBox_PilihSiswa.Name = "gBox_PilihSiswa";
            this.gBox_PilihSiswa.Size = new System.Drawing.Size(561, 333);
            this.gBox_PilihSiswa.TabIndex = 17;
            this.gBox_PilihSiswa.TabStop = false;
            this.gBox_PilihSiswa.Text = "Pilih Siswa";
            // 
            // dgv_DataSiswa
            // 
            this.dgv_DataSiswa.AllowUserToAddRows = false;
            this.dgv_DataSiswa.AllowUserToDeleteRows = false;
            this.dgv_DataSiswa.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_DataSiswa.Location = new System.Drawing.Point(6, 64);
            this.dgv_DataSiswa.Name = "dgv_DataSiswa";
            this.dgv_DataSiswa.Size = new System.Drawing.Size(541, 197);
            this.dgv_DataSiswa.TabIndex = 8;
            this.dgv_DataSiswa.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_DataSiswa_CellClick);
            // 
            // txtBox_NoHp
            // 
            this.txtBox_NoHp.Location = new System.Drawing.Point(75, 300);
            this.txtBox_NoHp.Name = "txtBox_NoHp";
            this.txtBox_NoHp.ReadOnly = true;
            this.txtBox_NoHp.Size = new System.Drawing.Size(172, 26);
            this.txtBox_NoHp.TabIndex = 7;
            // 
            // txtBox_AsalSekolah
            // 
            this.txtBox_AsalSekolah.Location = new System.Drawing.Point(260, 300);
            this.txtBox_AsalSekolah.Name = "txtBox_AsalSekolah";
            this.txtBox_AsalSekolah.ReadOnly = true;
            this.txtBox_AsalSekolah.Size = new System.Drawing.Size(172, 26);
            this.txtBox_AsalSekolah.TabIndex = 6;
            // 
            // txtBox_Nama
            // 
            this.txtBox_Nama.Location = new System.Drawing.Point(75, 268);
            this.txtBox_Nama.Name = "txtBox_Nama";
            this.txtBox_Nama.ReadOnly = true;
            this.txtBox_Nama.Size = new System.Drawing.Size(172, 26);
            this.txtBox_Nama.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(2, 303);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "No HP :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(256, 271);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Asal Sekolah :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 271);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Nama :";
            // 
            // btn_ImportFile
            // 
            this.btn_ImportFile.Location = new System.Drawing.Point(6, 25);
            this.btn_ImportFile.Name = "btn_ImportFile";
            this.btn_ImportFile.Size = new System.Drawing.Size(107, 33);
            this.btn_ImportFile.TabIndex = 1;
            this.btn_ImportFile.Text = "Import File";
            this.btn_ImportFile.UseVisualStyleBackColor = true;
            this.btn_ImportFile.Click += new System.EventHandler(this.btn_ImportFile_Click);
            // 
            // gBox_SMS
            // 
            this.gBox_SMS.Controls.Add(this.rchBox_TextSms);
            this.gBox_SMS.Controls.Add(this.label4);
            this.gBox_SMS.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gBox_SMS.Location = new System.Drawing.Point(13, 437);
            this.gBox_SMS.Name = "gBox_SMS";
            this.gBox_SMS.Size = new System.Drawing.Size(481, 174);
            this.gBox_SMS.TabIndex = 18;
            this.gBox_SMS.TabStop = false;
            this.gBox_SMS.Text = "SMS";
            // 
            // rchBox_TextSms
            // 
            this.rchBox_TextSms.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rchBox_TextSms.Location = new System.Drawing.Point(73, 25);
            this.rchBox_TextSms.MaxLength = 150;
            this.rchBox_TextSms.Name = "rchBox_TextSms";
            this.rchBox_TextSms.Size = new System.Drawing.Size(359, 140);
            this.rchBox_TextSms.TabIndex = 6;
            this.rchBox_TextSms.Text = "";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 20);
            this.label4.TabIndex = 5;
            this.label4.Text = "Text :";
            // 
            // btn_KirimSms
            // 
            this.btn_KirimSms.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_KirimSms.Location = new System.Drawing.Point(500, 518);
            this.btn_KirimSms.Name = "btn_KirimSms";
            this.btn_KirimSms.Size = new System.Drawing.Size(101, 33);
            this.btn_KirimSms.TabIndex = 7;
            this.btn_KirimSms.Text = "Send SMS";
            this.btn_KirimSms.UseVisualStyleBackColor = true;
            this.btn_KirimSms.Click += new System.EventHandler(this.btn_KirimSms_Click);
            // 
            // rdBtn_TidakTerkirim
            // 
            this.rdBtn_TidakTerkirim.AutoSize = true;
            this.rdBtn_TidakTerkirim.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdBtn_TidakTerkirim.Location = new System.Drawing.Point(500, 478);
            this.rdBtn_TidakTerkirim.Name = "rdBtn_TidakTerkirim";
            this.rdBtn_TidakTerkirim.Size = new System.Drawing.Size(124, 24);
            this.rdBtn_TidakTerkirim.TabIndex = 1;
            this.rdBtn_TidakTerkirim.Text = "Tidak Terkirim";
            this.rdBtn_TidakTerkirim.UseVisualStyleBackColor = true;
            this.rdBtn_TidakTerkirim.CheckedChanged += new System.EventHandler(this.rdBtn_TidakTerkirim_CheckedChanged);
            // 
            // rdBtn_Normal
            // 
            this.rdBtn_Normal.AutoSize = true;
            this.rdBtn_Normal.Checked = true;
            this.rdBtn_Normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdBtn_Normal.Location = new System.Drawing.Point(500, 448);
            this.rdBtn_Normal.Name = "rdBtn_Normal";
            this.rdBtn_Normal.Size = new System.Drawing.Size(77, 24);
            this.rdBtn_Normal.TabIndex = 0;
            this.rdBtn_Normal.TabStop = true;
            this.rdBtn_Normal.Text = "Normal";
            this.rdBtn_Normal.UseVisualStyleBackColor = true;
            this.rdBtn_Normal.CheckedChanged += new System.EventHandler(this.rdBtn_Normal_CheckedChanged);
            // 
            // gBox_SentItems
            // 
            this.gBox_SentItems.Controls.Add(this.btn_refresh);
            this.gBox_SentItems.Controls.Add(this.btn_GetData);
            this.gBox_SentItems.Controls.Add(this.lBox_SentItems);
            this.gBox_SentItems.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gBox_SentItems.Location = new System.Drawing.Point(593, 98);
            this.gBox_SentItems.Name = "gBox_SentItems";
            this.gBox_SentItems.Size = new System.Drawing.Size(176, 353);
            this.gBox_SentItems.TabIndex = 19;
            this.gBox_SentItems.TabStop = false;
            this.gBox_SentItems.Text = "Sentitems";
            // 
            // btn_GetData
            // 
            this.btn_GetData.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_GetData.Location = new System.Drawing.Point(9, 25);
            this.btn_GetData.Name = "btn_GetData";
            this.btn_GetData.Size = new System.Drawing.Size(156, 32);
            this.btn_GetData.TabIndex = 2;
            this.btn_GetData.Text = "Get Data";
            this.btn_GetData.UseVisualStyleBackColor = true;
            this.btn_GetData.Click += new System.EventHandler(this.btn_GetData_Click);
            // 
            // lBox_SentItems
            // 
            this.lBox_SentItems.FormattingEnabled = true;
            this.lBox_SentItems.ItemHeight = 20;
            this.lBox_SentItems.Location = new System.Drawing.Point(9, 63);
            this.lBox_SentItems.Name = "lBox_SentItems";
            this.lBox_SentItems.Size = new System.Drawing.Size(156, 244);
            this.lBox_SentItems.TabIndex = 1;
            // 
            // btn_refresh
            // 
            this.btn_refresh.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_refresh.Location = new System.Drawing.Point(9, 313);
            this.btn_refresh.Name = "btn_refresh";
            this.btn_refresh.Size = new System.Drawing.Size(156, 32);
            this.btn_refresh.TabIndex = 3;
            this.btn_refresh.Text = "Refresh";
            this.btn_refresh.UseVisualStyleBackColor = true;
            this.btn_refresh.Click += new System.EventHandler(this.btn_refresh_Click);
            // 
            // SmsGatewaySiswa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(781, 619);
            this.Controls.Add(this.btn_KirimSms);
            this.Controls.Add(this.gBox_SentItems);
            this.Controls.Add(this.rdBtn_TidakTerkirim);
            this.Controls.Add(this.gBox_SMS);
            this.Controls.Add(this.rdBtn_Normal);
            this.Controls.Add(this.gBox_PilihSiswa);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MinimizeBox = false;
            this.Name = "SmsGatewaySiswa";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SMS Gateway Siswa";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SmsGatewaySiswa_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.gBox_PilihSiswa.ResumeLayout(false);
            this.gBox_PilihSiswa.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_DataSiswa)).EndInit();
            this.gBox_SMS.ResumeLayout(false);
            this.gBox_SMS.PerformLayout();
            this.gBox_SentItems.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox gBox_PilihSiswa;
        private System.Windows.Forms.TextBox txtBox_NoHp;
        private System.Windows.Forms.TextBox txtBox_AsalSekolah;
        private System.Windows.Forms.TextBox txtBox_Nama;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_ImportFile;
        private System.Windows.Forms.GroupBox gBox_SMS;
        private System.Windows.Forms.RichTextBox rchBox_TextSms;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_KirimSms;
        private System.Windows.Forms.RadioButton rdBtn_TidakTerkirim;
        private System.Windows.Forms.RadioButton rdBtn_Normal;
        private System.Windows.Forms.GroupBox gBox_SentItems;
        private System.Windows.Forms.ListBox lBox_SentItems;
        private System.Windows.Forms.Button btn_GetData;
        private System.Windows.Forms.DataGridView dgv_DataSiswa;
        private System.Windows.Forms.Button btn_refresh;
    }
}