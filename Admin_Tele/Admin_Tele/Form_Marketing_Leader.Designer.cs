﻿namespace Admin_Tele
{
    partial class Form_Marketing_Leader
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.TxtBox_NamaMarketing = new System.Windows.Forms.TextBox();
            this.TxtBox_IDMarketing = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.Btn_Hapus = new System.Windows.Forms.Button();
            this.Btn_Ubah = new System.Windows.Forms.Button();
            this.Btn_Batal = new System.Windows.Forms.Button();
            this.Btn_Input = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.dataGridView1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(433, 180);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(6, 19);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(419, 155);
            this.dataGridView1.TabIndex = 5;
            this.dataGridView1.TabStop = false;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.TxtBox_NamaMarketing);
            this.groupBox2.Controls.Add(this.TxtBox_IDMarketing);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(149, 198);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(296, 79);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // TxtBox_NamaMarketing
            // 
            this.TxtBox_NamaMarketing.Location = new System.Drawing.Point(118, 48);
            this.TxtBox_NamaMarketing.Name = "TxtBox_NamaMarketing";
            this.TxtBox_NamaMarketing.Size = new System.Drawing.Size(171, 20);
            this.TxtBox_NamaMarketing.TabIndex = 0;
            // 
            // TxtBox_IDMarketing
            // 
            this.TxtBox_IDMarketing.Enabled = false;
            this.TxtBox_IDMarketing.Location = new System.Drawing.Point(118, 19);
            this.TxtBox_IDMarketing.Name = "TxtBox_IDMarketing";
            this.TxtBox_IDMarketing.Size = new System.Drawing.Size(100, 20);
            this.TxtBox_IDMarketing.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(5, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nama Marketing";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(29, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID Marketing";
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.Btn_Hapus);
            this.groupBox3.Controls.Add(this.Btn_Ubah);
            this.groupBox3.Controls.Add(this.Btn_Batal);
            this.groupBox3.Controls.Add(this.Btn_Input);
            this.groupBox3.Location = new System.Drawing.Point(115, 283);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(330, 50);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            // 
            // Btn_Hapus
            // 
            this.Btn_Hapus.Enabled = false;
            this.Btn_Hapus.Location = new System.Drawing.Point(249, 19);
            this.Btn_Hapus.Name = "Btn_Hapus";
            this.Btn_Hapus.Size = new System.Drawing.Size(75, 23);
            this.Btn_Hapus.TabIndex = 4;
            this.Btn_Hapus.Text = "Hapus";
            this.Btn_Hapus.UseVisualStyleBackColor = true;
            this.Btn_Hapus.Click += new System.EventHandler(this.Btn_Hapus_Click);
            // 
            // Btn_Ubah
            // 
            this.Btn_Ubah.Enabled = false;
            this.Btn_Ubah.Location = new System.Drawing.Point(168, 19);
            this.Btn_Ubah.Name = "Btn_Ubah";
            this.Btn_Ubah.Size = new System.Drawing.Size(75, 23);
            this.Btn_Ubah.TabIndex = 3;
            this.Btn_Ubah.Text = "Ubah";
            this.Btn_Ubah.UseVisualStyleBackColor = true;
            this.Btn_Ubah.Click += new System.EventHandler(this.Btn_Ubah_Click);
            // 
            // Btn_Batal
            // 
            this.Btn_Batal.Enabled = false;
            this.Btn_Batal.Location = new System.Drawing.Point(87, 19);
            this.Btn_Batal.Name = "Btn_Batal";
            this.Btn_Batal.Size = new System.Drawing.Size(75, 23);
            this.Btn_Batal.TabIndex = 2;
            this.Btn_Batal.Text = "Batal";
            this.Btn_Batal.UseVisualStyleBackColor = true;
            this.Btn_Batal.Click += new System.EventHandler(this.Btn_Batal_Click);
            // 
            // Btn_Input
            // 
            this.Btn_Input.Location = new System.Drawing.Point(6, 19);
            this.Btn_Input.Name = "Btn_Input";
            this.Btn_Input.Size = new System.Drawing.Size(75, 23);
            this.Btn_Input.TabIndex = 1;
            this.Btn_Input.Text = "Input";
            this.Btn_Input.UseVisualStyleBackColor = true;
            this.Btn_Input.Click += new System.EventHandler(this.Btn_Input_Click);
            // 
            // Form_Marketing_Leader
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Admin_Tele.Properties.Resources.Background3;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(457, 345);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "Form_Marketing_Leader";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Marketing Leader";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Marketing_Leader_FormClosing);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox TxtBox_NamaMarketing;
        private System.Windows.Forms.TextBox TxtBox_IDMarketing;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button Btn_Hapus;
        private System.Windows.Forms.Button Btn_Ubah;
        private System.Windows.Forms.Button Btn_Batal;
        private System.Windows.Forms.Button Btn_Input;
    }
}