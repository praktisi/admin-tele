﻿namespace Admin_Tele
{
    partial class Form_Report_Persekolah
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.DGView_AllData = new System.Windows.Forms.DataGridView();
            this.DGView_Hasil = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Btn_Export = new System.Windows.Forms.Button();
            this.Btn_Chart = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.DGView_Persentase = new System.Windows.Forms.DataGridView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.Cmb_AsalSekolah = new System.Windows.Forms.ComboBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Cmb_TahunPromosi = new System.Windows.Forms.ComboBox();
            this.Sfd_ExportPersekolah = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.DGView_AllData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGView_Hasil)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGView_Persentase)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(439, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(427, 33);
            this.label1.TabIndex = 0;
            this.label1.Text = "Data Seluruh Sesi Peneleponan";
            // 
            // DGView_AllData
            // 
            this.DGView_AllData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGView_AllData.Location = new System.Drawing.Point(6, 19);
            this.DGView_AllData.Name = "DGView_AllData";
            this.DGView_AllData.ReadOnly = true;
            this.DGView_AllData.Size = new System.Drawing.Size(1248, 247);
            this.DGView_AllData.TabIndex = 1;
            this.DGView_AllData.TabStop = false;
            // 
            // DGView_Hasil
            // 
            this.DGView_Hasil.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGView_Hasil.Location = new System.Drawing.Point(6, 19);
            this.DGView_Hasil.Name = "DGView_Hasil";
            this.DGView_Hasil.ReadOnly = true;
            this.DGView_Hasil.Size = new System.Drawing.Size(518, 306);
            this.DGView_Hasil.TabIndex = 3;
            this.DGView_Hasil.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(85, 314);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(366, 25);
            this.label2.TabIndex = 2;
            this.label2.Text = "Hasil Dari Seluruh Sesi Peneleponan";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(598, 314);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(427, 25);
            this.label3.TabIndex = 5;
            this.label3.Text = "Persentase Dari Seluruh Sesi Peneleponan";
            // 
            // Btn_Export
            // 
            this.Btn_Export.Enabled = false;
            this.Btn_Export.Location = new System.Drawing.Point(6, 145);
            this.Btn_Export.Name = "Btn_Export";
            this.Btn_Export.Size = new System.Drawing.Size(84, 37);
            this.Btn_Export.TabIndex = 3;
            this.Btn_Export.Text = "Export to Excel";
            this.Btn_Export.UseVisualStyleBackColor = true;
            this.Btn_Export.Click += new System.EventHandler(this.Btn_Export_Click);
            // 
            // Btn_Chart
            // 
            this.Btn_Chart.Enabled = false;
            this.Btn_Chart.Location = new System.Drawing.Point(97, 145);
            this.Btn_Chart.Name = "Btn_Chart";
            this.Btn_Chart.Size = new System.Drawing.Size(92, 36);
            this.Btn_Chart.TabIndex = 4;
            this.Btn_Chart.Text = "Tampilkan Grafik";
            this.Btn_Chart.UseVisualStyleBackColor = true;
            this.Btn_Chart.Click += new System.EventHandler(this.Btn_Chart_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.DGView_Hasil);
            this.groupBox1.Location = new System.Drawing.Point(12, 325);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(530, 331);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.DGView_Persentase);
            this.groupBox2.Location = new System.Drawing.Point(548, 325);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(530, 331);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            // 
            // DGView_Persentase
            // 
            this.DGView_Persentase.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGView_Persentase.Location = new System.Drawing.Point(6, 19);
            this.DGView_Persentase.Name = "DGView_Persentase";
            this.DGView_Persentase.ReadOnly = true;
            this.DGView_Persentase.Size = new System.Drawing.Size(518, 306);
            this.DGView_Persentase.TabIndex = 3;
            this.DGView_Persentase.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.DGView_AllData);
            this.groupBox3.Location = new System.Drawing.Point(12, 24);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1260, 272);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            // 
            // Cmb_AsalSekolah
            // 
            this.Cmb_AsalSekolah.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_AsalSekolah.Enabled = false;
            this.Cmb_AsalSekolah.FormattingEnabled = true;
            this.Cmb_AsalSekolah.Location = new System.Drawing.Point(6, 90);
            this.Cmb_AsalSekolah.Name = "Cmb_AsalSekolah";
            this.Cmb_AsalSekolah.Size = new System.Drawing.Size(183, 21);
            this.Cmb_AsalSekolah.TabIndex = 2;
            this.Cmb_AsalSekolah.SelectedIndexChanged += new System.EventHandler(this.Cmb_AsalSekolah_SelectedIndexChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.Transparent;
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.Cmb_TahunPromosi);
            this.groupBox4.Controls.Add(this.Btn_Chart);
            this.groupBox4.Controls.Add(this.Cmb_AsalSekolah);
            this.groupBox4.Controls.Add(this.Btn_Export);
            this.groupBox4.Location = new System.Drawing.Point(1084, 325);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(196, 188);
            this.groupBox4.TabIndex = 13;
            this.groupBox4.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 71);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(116, 16);
            this.label5.TabIndex = 15;
            this.label5.Text = "Pilih Asal Sekolah";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(127, 16);
            this.label4.TabIndex = 14;
            this.label4.Text = "Pilih Tahun Promosi";
            // 
            // Cmb_TahunPromosi
            // 
            this.Cmb_TahunPromosi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_TahunPromosi.FormattingEnabled = true;
            this.Cmb_TahunPromosi.Location = new System.Drawing.Point(7, 38);
            this.Cmb_TahunPromosi.Name = "Cmb_TahunPromosi";
            this.Cmb_TahunPromosi.Size = new System.Drawing.Size(183, 21);
            this.Cmb_TahunPromosi.TabIndex = 1;
            this.Cmb_TahunPromosi.SelectedIndexChanged += new System.EventHandler(this.Cmb_TahunPromosi_SelectedIndexChanged);
            // 
            // Form_Report_Persekolah
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Admin_Tele.Properties.Resources.images__5_;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1284, 668);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "Form_Report_Persekolah";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Report Persekolah";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Report_Persekolah_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.DGView_AllData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGView_Hasil)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGView_Persentase)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView DGView_AllData;
        private System.Windows.Forms.DataGridView DGView_Hasil;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button Btn_Export;
        private System.Windows.Forms.Button Btn_Chart;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView DGView_Persentase;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox Cmb_AsalSekolah;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.SaveFileDialog Sfd_ExportPersekolah;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox Cmb_TahunPromosi;
    }
}