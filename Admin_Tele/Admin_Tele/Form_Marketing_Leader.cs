﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Admin_Tele
{
    public partial class Form_Marketing_Leader : Form
    {
        MySqlConnection conn = new MySqlConnection(Constanta.conString);
        MySqlCommand cmd;
        MySqlDataAdapter adpt;
        MySqlDataReader dReader;
        string query;

        public Form_Marketing_Leader()
        {
            InitializeComponent();
            TxtBox_NamaMarketing.Focus();
            TxtBox_IDMarketing.Text = GetCurrentIDFromDB();
            DesignGrid();
            ShowData();
        }

        string GetCurrentIDFromDB()
        {
            conn.Close();
            string calonID = "";
            string idTerakhir = "";

            query = "SELECT id_marketing FROM tb_marketing ORDER BY id_marketing DESC LIMIT 0,1";
            cmd = new MySqlCommand(query, conn);

            try
            {
                conn.Open();
                dReader = cmd.ExecuteReader();

                if (dReader.Read())
                {
                    idTerakhir = dReader[0].ToString();

                    int i = int.Parse(idTerakhir);

                    int newID = i + 1;

                    calonID = idTerakhir.Substring(0, 3 - newID.ToString().Length) + newID;
                }
                else
                {
                    calonID = "001";
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return calonID;
        }

        private void DesignGrid()
        {
            dataGridView1.ColumnCount = 2;
            dataGridView1.Columns[0].Name = "ID Marketing";
            dataGridView1.Columns[1].Name = "Nama Marketing";
            dataGridView1.Columns[1].Width = 275;

            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.AllowUserToResizeColumns = false;
        }

        private void FillTextBox()
        {
            string idMar = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            string namaMar = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();

            TxtBox_IDMarketing.Text = idMar;
            TxtBox_NamaMarketing.Text = namaMar;
        }

        private void ShowData()
        {
            dataGridView1.Rows.Clear();

            query = "SELECT * FROM tb_marketing";
            cmd = new MySqlCommand(query, conn);

            try
            {
                conn.Open();
                DataTable dt = new DataTable();
                adpt = new MySqlDataAdapter(cmd);
                adpt.Fill(dt);

                foreach (DataRow r in dt.Rows)
                {
                    dataGridView1.Rows.Add(r[0].ToString(), r[1].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ClearText()
        {
            TxtBox_IDMarketing.Text = GetCurrentIDFromDB();
            TxtBox_NamaMarketing.Text = "";

            Btn_Input.Enabled = true;
            Btn_Ubah.Enabled = false;
            Btn_Hapus.Enabled = false;
            Btn_Batal.Enabled = false;
        }

        private void InputData()
        {
            query = "INSERT INTO tb_marketing(id_marketing, nama_marketing) VALUES(@id, @nama)";
            cmd = new MySqlCommand(query, conn);

            cmd.Parameters.AddWithValue("@id", TxtBox_IDMarketing.Text);
            cmd.Parameters.AddWithValue("@nama", TxtBox_NamaMarketing.Text);

            try
            {
                conn.Open();
                if (cmd.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("Marketing " + TxtBox_NamaMarketing.Text + " telah berhasil ditambahkan");
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void UbahData(string id, string nama)
        {
            query = "UPDATE tb_marketing SET nama_marketing ='" + nama + "' WHERE id_marketing ='" + id + "'";
            cmd = new MySqlCommand(query, conn);

            try
            {
                conn.Open();

                adpt = new MySqlDataAdapter(cmd);
                adpt.UpdateCommand = conn.CreateCommand();
                adpt.UpdateCommand.CommandText = query;

                if (cmd.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("Data marketing " + nama + " telah berhasil diubah");
                }
                conn.Close();

                ShowData();
                ClearText();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void HapusData(string id)
        {
            query = "DELETE FROM tb_marketing WHERE id_marketing = '" + id + "'";
            cmd = new MySqlCommand(query, conn);

            try
            {
                conn.Open();

                adpt = new MySqlDataAdapter(cmd);
                adpt.UpdateCommand = conn.CreateCommand();
                adpt.UpdateCommand.CommandText = query;

                if (cmd.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("Data marketing " + TxtBox_NamaMarketing.Text + " telah berhasil dihapus dari list marketing");
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Btn_Input_Click(object sender, EventArgs e)
        {
            if(TxtBox_NamaMarketing.Text == "")
            {
                MessageBox.Show("Harap isi nama marketing lebih dahulu");
            }
            else
            {
                InputData();
                ShowData();
                ClearText();
            }
        }

        private void Btn_Batal_Click(object sender, EventArgs e)
        {
            ClearText();
        }

        private void Btn_Ubah_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Apakah anda akin akan mengubah data telemarketing " + TxtBox_NamaMarketing.Text + " ?",
                    "Ubah Data", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                if(TxtBox_NamaMarketing.Text != "")
                {
                    UbahData(TxtBox_IDMarketing.Text, TxtBox_NamaMarketing.Text);
                    ShowData();
                    ClearText();
                }
                else
                {
                    MessageBox.Show("Maaf nama marketing tidak boleh kosong");
                }
            }
        }

        private void Btn_Hapus_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Apakah anda akin akan mengubah data telemarketing " + TxtBox_NamaMarketing.Text + " ?",
                    "Hapus Data", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                HapusData(TxtBox_IDMarketing.Text);
                ShowData();
                ClearText();
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            FillTextBox();

            Btn_Input.Enabled = false;
            Btn_Ubah.Enabled = true;
            Btn_Hapus.Enabled = true;
            Btn_Batal.Enabled = true;
        }

        private void Form_Marketing_Leader_FormClosing(object sender, FormClosingEventArgs e)
        {
            Main.isChildFormAppeared = !Main.isChildFormAppeared;
        }
    }
}
