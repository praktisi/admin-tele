﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Excel = Microsoft.Office.Interop.Excel;

 

namespace Admin_Tele
{
    public partial class Form_Report_Persekolah : Form
    {
        string query;
        public static List<string> sesi1 = new List<string>();
        public static List<string> sesi2 = new List<string>();
        public static List<string> sesi3 = new List<string>();
        public static List<string> sesi4 = new List<string>();
        public static List<string> sesi5 = new List<string>();
        public static List<string> katJaw = new List<string>();
        public static List<int> hasilSesi1 = new List<int>();
        public static List<int> hasilSesi2 = new List<int>();
        public static List<int> hasilSesi3 = new List<int>();
        public static List<int> hasilSesi4 = new List<int>();
        public static List<int> hasilSesi5 = new List<int>();
        public static List<int> hasilTotal = new List<int>();
        public static List<string> katjawSesi1 = new List<string>();
        public static List<string> katjawSesi2 = new List<string>();
        public static List<string> katjawSesi3 = new List<string>();
        public static List<string> katjawSesi4 = new List<string>();
        public static List<string> katjawSesi5 = new List<string>();
        public static List<string> katjawTotal = new List<string>();

        MySqlConnection conn = new MySqlConnection(Constanta.conString);
        MySqlCommand cmd;
        MySqlDataAdapter adpt;

        Excel.Application xlApp;
        Excel.Workbook xlWb;
        Excel.Worksheet xlWs;

        public Form_Report_Persekolah()
        {
            InitializeComponent();

            DesignDGViewAllData();
            DesignDGViewHasil();
            DesignDGViewPersentase();
            FillTahunPromo();
        }

        private void DesignDGViewAllData()
        {
            DGView_AllData.ColumnCount = 14;
            DGView_AllData.Columns[0].Name = "No.";
            DGView_AllData.Columns[0].Width = 30;
            DGView_AllData.Columns[1].Name = "Nama";
            DGView_AllData.Columns[1].Width = 150;
            DGView_AllData.Columns[2].Name = "No Telepon";
            DGView_AllData.Columns[2].Width = 90;
            DGView_AllData.Columns[3].Name = "Tanggal Sesi 1";
            DGView_AllData.Columns[3].Width = 105;
            DGView_AllData.Columns[4].Name = "Sesi 1";
            DGView_AllData.Columns[4].Width = 200;
            DGView_AllData.Columns[5].Name = "Tanggal Sesi 2";
            DGView_AllData.Columns[5].Width = 105;
            DGView_AllData.Columns[6].Name = "Sesi 2";
            DGView_AllData.Columns[6].Width = 200;
            DGView_AllData.Columns[7].Name = "Tanggal Sesi 3";
            DGView_AllData.Columns[7].Width = 105;
            DGView_AllData.Columns[8].Name = "Sesi 3";
            DGView_AllData.Columns[8].Width = 200;
            DGView_AllData.Columns[9].Name = "Tanggal Sesi 4";
            DGView_AllData.Columns[9].Width = 105;
            DGView_AllData.Columns[10].Name = "Sesi 4";
            DGView_AllData.Columns[10].Width = 200;
            DGView_AllData.Columns[11].Name = "Tanggal Sesi 5";
            DGView_AllData.Columns[11].Width = 105;
            DGView_AllData.Columns[12].Name = "Sesi 5";
            DGView_AllData.Columns[12].Width = 200;
            DGView_AllData.Columns[13].Name = "Hasil";

            DGView_AllData.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            DGView_AllData.MultiSelect = false;
            DGView_AllData.AllowUserToAddRows = false;
            DGView_AllData.AllowUserToResizeColumns = false;
        }

        private void DesignDGViewHasil()
        {
            DGView_Hasil.ColumnCount = 8;
            DGView_Hasil.Columns[0].Name = "No.";
            DGView_Hasil.Columns[0].Width = 28;
            DGView_Hasil.Columns[1].Name = "Kategori Jawaban";
            DGView_Hasil.Columns[1].Width = 150;
            DGView_Hasil.Columns[2].Name = "Sesi 1";
            DGView_Hasil.Columns[2].Width = 50;
            DGView_Hasil.Columns[3].Name = "Sesi 2";
            DGView_Hasil.Columns[3].Width = 50;
            DGView_Hasil.Columns[4].Name = "Sesi 3";
            DGView_Hasil.Columns[4].Width = 50;
            DGView_Hasil.Columns[5].Name = "Sesi 4";
            DGView_Hasil.Columns[5].Width = 50;
            DGView_Hasil.Columns[6].Name = "Sesi 5";
            DGView_Hasil.Columns[6].Width = 50;
            DGView_Hasil.Columns[7].Name = "Total";
            DGView_Hasil.Columns[7].Width = 50;

            DGView_Hasil.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            DGView_Hasil.MultiSelect = false;
            DGView_Hasil.AllowUserToAddRows = false;
            DGView_Hasil.AllowUserToResizeColumns = false;
        }

        private void DesignDGViewPersentase()
        {
            DGView_Persentase.ColumnCount = 8;
            DGView_Persentase.Columns[0].Name = "No.";
            DGView_Persentase.Columns[0].Width = 28;
            DGView_Persentase.Columns[1].Name = "Kategori Jawaban";
            DGView_Persentase.Columns[1].Width = 150;
            DGView_Persentase.Columns[2].Name = "Sesi 1";
            DGView_Persentase.Columns[2].Width = 50;
            DGView_Persentase.Columns[3].Name = "Sesi 2";
            DGView_Persentase.Columns[3].Width = 50;
            DGView_Persentase.Columns[4].Name = "Sesi 3";
            DGView_Persentase.Columns[4].Width = 50;
            DGView_Persentase.Columns[5].Name = "Sesi 4";
            DGView_Persentase.Columns[5].Width = 50;
            DGView_Persentase.Columns[6].Name = "Sesi 5";
            DGView_Persentase.Columns[6].Width = 50;
            DGView_Persentase.Columns[7].Name = "Total";
            DGView_Persentase.Columns[7].Width = 50;

            DGView_Persentase.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            DGView_Persentase.MultiSelect = false;
            DGView_Persentase.AllowUserToAddRows = false;
            DGView_Persentase.AllowUserToResizeColumns = false;
            DGView_Persentase.AutoResizeColumns();
        }

        private void FillTahunPromo()
        {
            query = "SELECT DISTINCT tahun_promosi FROM tb_sesipeneleponan";
            cmd = new MySqlCommand(query, conn);

            try
            {
                conn.Open();

                DataTable dt = new DataTable();
                adpt = new MySqlDataAdapter(cmd);
                adpt.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    Cmb_TahunPromosi.Items.Add(r[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void FillCmbAsalSekolah()
        {
            query = "SELECT DISTINCT asal_sekolah FROM tb_sesipeneleponan WHERE tahun_promosi = '" + Cmb_TahunPromosi.Text + "' AND id_marketing !=''";
            cmd = new MySqlCommand(query, conn);

            try
            {
                conn.Open();
                DataTable dt = new DataTable();
                adpt = new MySqlDataAdapter(cmd);
                adpt.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    Cmb_AsalSekolah.Items.Add(r[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
                conn.Dispose();
            }
        }

        private void ShowAllData(string asalSklh)
        {
            sesi1.Clear();
            sesi2.Clear();
            sesi3.Clear();
            sesi4.Clear();
            sesi5.Clear();

            DGView_AllData.Rows.Clear();
            string query = "SELECT * FROM tb_sesipeneleponan WHERE asal_sekolah = '" + asalSklh + "'";
            cmd = new MySqlCommand(query, conn);

            try
            {
                conn.Open();
                DataTable dt = new DataTable();
                adpt = new MySqlDataAdapter(cmd);
                adpt.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    DGView_AllData.Rows.Add(r[1].ToString(), r[2].ToString(), r[3].ToString(), r[8].ToString(), r[7].ToString(),  
                        r[10].ToString(), r[9].ToString(), r[12].ToString(), r[11].ToString(), r[14].ToString(), r[13].ToString(),
                        r[16].ToString(), r[15].ToString(), r[17].ToString());
                    sesi1.Add(r[7].ToString());
                    sesi2.Add(r[9].ToString());
                    sesi3.Add(r[11].ToString());
                    sesi4.Add(r[13].ToString());
                    sesi5.Add(r[15].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ShowHasilData()
        {
            int sumSesi1 = 0;
            int sumSesi2 = 0;
            int sumSesi3 = 0;
            int sumSesi4 = 0;
            int sumSesi5 = 0;
            int sumAllSesi = 0;
            hasilSesi1.Clear();
            hasilSesi2.Clear();
            hasilSesi3.Clear();
            hasilSesi4.Clear();
            hasilSesi5.Clear();
            hasilTotal.Clear();
            katJaw.Clear();
            katjawSesi1.Clear();
            katjawSesi2.Clear();
            katjawSesi3.Clear();
            katjawSesi4.Clear();
            katjawSesi5.Clear();
            katjawTotal.Clear();

            DGView_Hasil.Rows.Clear();
            DGView_Persentase.Rows.Clear();

            string query = "SELECT * FROM tb_kategorijawaban";
            cmd = new MySqlCommand(query, conn);
            try
            {
                conn.Open();
                DataTable dt = new DataTable();
                adpt = new MySqlDataAdapter(cmd);
                adpt.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    DGView_Hasil.Rows.Add(r[0].ToString(), r[1].ToString(), 0, 0, 0, 0, 0, 0);
                    DGView_Persentase.Rows.Add(r[0].ToString(), r[1].ToString());
                    katJaw.Add(r[1].ToString());
                }
                conn.Close();

                //Mengisi hasil data dari masing - masing sesi peneleponan
                for (int anakKe = 0; anakKe < DGView_AllData.RowCount; anakKe++)
                {
                    for (int j = 0; j < DGView_Hasil.RowCount; j++)
                    {
                        if (sesi1[anakKe] == DGView_Hasil.Rows[j].Cells[1].Value.ToString())
                        {
                            DGView_Hasil.Rows[j].Cells[2].Value = (int.Parse(DGView_Hasil.Rows[j].Cells[2].Value.ToString()) + 1);
                            sumSesi1 = sumSesi1 + 1;
                        }

                        if (sesi2[anakKe] == DGView_Hasil.Rows[j].Cells[1].Value.ToString())
                        {
                            DGView_Hasil.Rows[j].Cells[3].Value = (int.Parse(DGView_Hasil.Rows[j].Cells[3].Value.ToString()) + 1);
                            sumSesi2 = sumSesi2 + 1;
                        }

                        if (sesi3[anakKe] == DGView_Hasil.Rows[j].Cells[1].Value.ToString())
                        {
                            DGView_Hasil.Rows[j].Cells[4].Value = (int.Parse(DGView_Hasil.Rows[j].Cells[4].Value.ToString()) + 1);
                            sumSesi3 = sumSesi3 + 1;
                        }

                        if (sesi4[anakKe] == DGView_Hasil.Rows[j].Cells[1].Value.ToString())
                        {
                            DGView_Hasil.Rows[j].Cells[5].Value = (int.Parse(DGView_Hasil.Rows[j].Cells[5].Value.ToString()) + 1);
                            sumSesi4 = sumSesi4 + 1;
                        } 
                        
                        if (sesi5[anakKe] == DGView_Hasil.Rows[j].Cells[1].Value.ToString())
                        {
                            DGView_Hasil.Rows[j].Cells[6].Value = (int.Parse(DGView_Hasil.Rows[j].Cells[6].Value.ToString()) + 1);
                            sumSesi5 = sumSesi5 + 1;
                        }
                        //Mengisi total data dari tiap - tiap sesi
                        DGView_Hasil.Rows[j].Cells[7].Value = int.Parse(DGView_Hasil.Rows[j].Cells[2].Value.ToString()) +
                            int.Parse(DGView_Hasil.Rows[j].Cells[3].Value.ToString()) + int.Parse(DGView_Hasil.Rows[j].Cells[4].Value.ToString()) +
                            int.Parse(DGView_Hasil.Rows[j].Cells[5].Value.ToString()) + int.Parse(DGView_Hasil.Rows[j].Cells[6].Value.ToString());
                    }
                }

                sumAllSesi = sumSesi1 + sumSesi2 + sumSesi3 + sumSesi4 + sumSesi5;

                for (int i = 0; i < DGView_Hasil.Rows.Count; i++)
                {
                    if (int.Parse(DGView_Hasil.Rows[i].Cells[2].Value.ToString()) != 0)
                    {
                        hasilSesi1.Add(int.Parse(DGView_Hasil.Rows[i].Cells[2].Value.ToString()));
                        katjawSesi1.Add(DGView_Hasil.Rows[i].Cells[1].Value.ToString());
                    }
                }

                for (int i = 0; i < DGView_Hasil.Rows.Count; i++)
                {
                    if (int.Parse(DGView_Hasil.Rows[i].Cells[3].Value.ToString()) != 0)
                    {
                        hasilSesi2.Add(int.Parse(DGView_Hasil.Rows[i].Cells[3].Value.ToString())); 
                        katjawSesi2.Add(DGView_Hasil.Rows[i].Cells[1].Value.ToString());
                    }
                }

                for (int i = 0; i < DGView_Hasil.Rows.Count; i++)
                {
                    if (int.Parse(DGView_Hasil.Rows[i].Cells[4].Value.ToString()) != 0)
                    {
                        hasilSesi3.Add(int.Parse(DGView_Hasil.Rows[i].Cells[4].Value.ToString()));
                        katjawSesi3.Add(DGView_Hasil.Rows[i].Cells[1].Value.ToString());
                    }
                }

                for (int i = 0; i < DGView_Hasil.Rows.Count; i++)
                {
                    if (int.Parse(DGView_Hasil.Rows[i].Cells[5].Value.ToString()) != 0)
                    {
                        hasilSesi4.Add(int.Parse(DGView_Hasil.Rows[i].Cells[5].Value.ToString()));
                        katjawSesi4.Add(DGView_Hasil.Rows[i].Cells[1].Value.ToString());
                    }
                }

                for (int i = 0; i < DGView_Hasil.Rows.Count; i++)
                {
                    if (int.Parse(DGView_Hasil.Rows[i].Cells[6].Value.ToString()) != 0)
                    {
                        hasilSesi5.Add(int.Parse(DGView_Hasil.Rows[i].Cells[6].Value.ToString()));
                        katjawSesi5.Add(DGView_Hasil.Rows[i].Cells[1].Value.ToString());
                    }
                }

                for (int i = 0; i < DGView_Hasil.Rows.Count; i++)
                {
                    if (int.Parse(DGView_Hasil.Rows[i].Cells[7].Value.ToString()) != 0)
                    {
                        hasilTotal.Add(int.Parse(DGView_Hasil.Rows[i].Cells[7].Value.ToString()));
                        katjawTotal.Add(DGView_Hasil.Rows[i].Cells[1].Value.ToString());
                    }
                }

                //Menambahkan baris total dan mengisi baris total jumlah hasil data
                DGView_Hasil.Rows.Add("", "Total: ", sumSesi1, sumSesi2, sumSesi3, sumSesi4, sumSesi5, sumAllSesi);
                DGView_Persentase.Rows.Add("", "Total: ");

                //Mengisi persentase dari hasil data
                for (int i = 0; i < DGView_Hasil.RowCount; i++)
                {
                    if(sumSesi1 != 0)
                    {
                        DGView_Persentase.Rows[i].Cells[2].Value = (float.Parse(DGView_Hasil.Rows[i].Cells[2].Value.ToString()) / sumSesi1);
                    }
                    else
                    {
                        DGView_Persentase.Rows[i].Cells[2].Value = 0;
                    }

                    if (sumSesi2 != 0)
                    {
                        DGView_Persentase.Rows[i].Cells[3].Value = (float.Parse(DGView_Hasil.Rows[i].Cells[3].Value.ToString()) / sumSesi2);
                    }
                    else
                    {
                        DGView_Persentase.Rows[i].Cells[3].Value = 0;
                    }

                    if (sumSesi3 != 0)
                    {
                        DGView_Persentase.Rows[i].Cells[4].Value = (float.Parse(DGView_Hasil.Rows[i].Cells[4].Value.ToString()) / sumSesi3);
                    }
                    else
                    {
                        DGView_Persentase.Rows[i].Cells[4].Value = 0;
                    }

                    if (sumSesi4 != 0)
                    {
                        DGView_Persentase.Rows[i].Cells[5].Value = (float.Parse(DGView_Hasil.Rows[i].Cells[5].Value.ToString()) / sumSesi4);
                    }
                    else
                    {
                        DGView_Persentase.Rows[i].Cells[5].Value = 0;
                    }

                    if (sumSesi5 != 0)
                    {
                        DGView_Persentase.Rows[i].Cells[6].Value = (float.Parse(DGView_Hasil.Rows[i].Cells[6].Value.ToString()) / sumSesi5);
                    }
                    else
                    {
                        DGView_Persentase.Rows[i].Cells[6].Value = 0;
                    }

                    //Mengisi total data dari tiap - tiap sesi
                    DGView_Persentase.Rows[i].Cells[7].Value = (float.Parse(DGView_Hasil.Rows[i].Cells[7].Value.ToString()) / sumAllSesi);
                }

                //Format DataGrid menjadi persen dan 2 angka dibelakang koma
                for(int i = 2; i < 8; i++)
                {
                    DGView_Persentase.Columns[i].DefaultCellStyle.Format = "P0";
                    DGView_Persentase.Columns[i].DefaultCellStyle.Format = "P2";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Cmb_AsalSekolah_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(Cmb_AsalSekolah.Text != "")
            {
                ShowAllData(Cmb_AsalSekolah.Text);
                ShowHasilData();
                Btn_Chart.Enabled = true;
                Btn_Export.Enabled = true;
            }
        }

        private void Export_Excel_AllData(DataGridView dg)
        {
            object missValue = System.Reflection.Missing.Value;

            // Pengaturan excel
            xlApp = new Excel.Application();
            xlApp.SheetsInNewWorkbook = 3;
            xlWb = xlApp.Workbooks.Add(missValue);

            #region Template_Tabulasi_data
            xlWs = (Excel.Worksheet)xlWb.Worksheets.get_Item(1);
            xlWs.Name = "Tabulasi Data";
            xlWs.Tab.Color = Color.Green;

            // merge cells
            //xlWs.get_Range("A6 : A8; B6 : B8; C6 : C8; D6 : M6; N6 : N8; D7 : E7; F7 : G7; 
            //H7 : I7; J7 : K7; L7 : M7; A1 : N1", Type.Missing).Merge(Type.Missing);
            // membuat merge cell - 2
            xlWs.Range[xlWs.Cells[6, 1], xlWs.Cells[8, 1]].Merge();
            xlWs.Range[xlWs.Cells[6, 2], xlWs.Cells[8, 2]].Merge();
            xlWs.Range[xlWs.Cells[6, 3], xlWs.Cells[8, 3]].Merge();
            xlWs.Range[xlWs.Cells[6, 4], xlWs.Cells[6, 13]].Merge();
            xlWs.Range[xlWs.Cells[7, 4], xlWs.Cells[7, 5]].Merge();
            xlWs.Range[xlWs.Cells[7, 6], xlWs.Cells[7, 7]].Merge();
            xlWs.Range[xlWs.Cells[7, 8], xlWs.Cells[7, 9]].Merge();
            xlWs.Range[xlWs.Cells[7, 10], xlWs.Cells[7, 11]].Merge();
            xlWs.Range[xlWs.Cells[7, 12], xlWs.Cells[7, 13]].Merge();
            xlWs.Range[xlWs.Cells[6, 14], xlWs.Cells[8, 14]].Merge();

            // memberi border
            xlWs.get_Range("A6 : N8").Cells.Borders.Weight = Excel.XlBorderWeight.xlMedium;
            xlWs.get_Range("A9 : N2000").Cells.Borders.Weight = Excel.XlBorderWeight.xlThin;
            
            //// memberi Width
            xlWs.get_Range("B6").ColumnWidth = 19;
            xlWs.get_Range("C6").ColumnWidth = 17;
            xlWs.get_Range("D8").ColumnWidth = 14;
            xlWs.get_Range("F8").ColumnWidth = 14;
            xlWs.get_Range("H8").ColumnWidth = 14;
            xlWs.get_Range("J8").ColumnWidth = 14;
            xlWs.get_Range("L8").ColumnWidth = 14;
            xlWs.get_Range("E8").ColumnWidth = 29;
            xlWs.get_Range("G8").ColumnWidth = 29;
            xlWs.get_Range("I8").ColumnWidth = 29;
            xlWs.get_Range("K8").ColumnWidth = 29;
            xlWs.get_Range("M8").ColumnWidth = 29;
            xlWs.get_Range("N8").ColumnWidth = 20;
            
            // memberi warna
            xlWs.get_Range("A6 : N8").Cells.Interior.Color = Color.AliceBlue;
            
            // justify
            xlWs.get_Range("A6 : N8").Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            xlWs.get_Range("A6 : N8").Cells.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
            xlWs.get_Range("A1 : N1").Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            xlWs.get_Range("A1 : N1").Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
            //xlWs.get_Range("A9 : A2000; C9 : C2000; D9 : D2000; F9 : F2000; H9 : H2000; J9 : J2000; L9 : L2000; N9 : N2000").Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            xlWs.get_Range("A9 : A2000").Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

            // font bold
            xlWs.get_Range("A6 : N8").Cells.Font.Bold = true;
            xlWs.get_Range("A6 : N8").Cells.Font.Size = 14;
            xlWs.get_Range("A1").Cells.Font.Bold = true;
            // Number Formating
            xlWs.get_Range("C6 : C2000").Columns.NumberFormat = "@";
            //xlWs.get_Range("D8 : D2000 ; F8 : F2000 ; H8 : H2000 ; J8 : J2000").Columns.NumberFormat = "dd/mm/yyyy";
            xlWs.get_Range("D8 : D2000").Columns.NumberFormat = "dd/mm/yyyy";

            // Mengisi cells Header
            xlWs.Cells[1, 1] = "Report " + Cmb_AsalSekolah.Text;
            xlWs.Cells[6, 1] = "NO";
            xlWs.Cells[6, 2] = "Nama";
            xlWs.Cells[6, 3] = "No Telpon";
            xlWs.Cells[6, 4] = "Sesi Peneleponan Ke - ";
            xlWs.Cells[6, 14] = "Status Sesi";
            xlWs.Cells[7, 4] = " 1 ";
            xlWs.Cells[7, 6] = " 2 ";
            xlWs.Cells[7, 8] = " 3 ";
            xlWs.Cells[7, 10] = " 4 ";
            xlWs.Cells[7, 12] = " 5 ";
            xlWs.Cells[8, 4] = "Tanggal";
            xlWs.Cells[8, 5] = "Hasil";
            xlWs.Cells[8, 6] = "Tanggal";
            xlWs.Cells[8, 7] = "Hasil";
            xlWs.Cells[8, 8] = "Tanggal";
            xlWs.Cells[8, 9] = "Hasil";
            xlWs.Cells[8, 10] = "Tanggal";
            xlWs.Cells[8, 11] = "Hasil";
            xlWs.Cells[8, 12] = "Tanggal";
            xlWs.Cells[8, 13] = "Hasil";

            // pengisian data dari datagrid
            try
            {
                for (int i = 0; i < dg.Rows.Count; i++)
                {
                    for (int j = 0; j < dg.Columns.Count; j++)
                    {
                        if (i <= dg.Rows.Count)
                            xlApp.Cells[i + 9, j + 1] = dg.Rows[i].Cells[j].Value.ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            #endregion
        }

        private string UciLieur(string s)
        {
            return string.Format("{0:0.##}", s);
        }

        private void Export_Excel_Hasil(DataGridView dg2, DataGridView dg3)
        {
            int i;
            int j;

            xlWs = (Excel.Worksheet)xlWb.Worksheets.get_Item(2);
            xlWs.Name = "Report Table";
            xlWs.Tab.Color = Color.Blue;
            
            // memberi border
            xlWs.get_Range("A6 : N7").Cells.Borders.Weight = Excel.XlBorderWeight.xlMedium;
            xlWs.get_Range("A8 : N39").Cells.Borders.Weight = Excel.XlBorderWeight.xlThin;
            
            // membuat marge cell - 1
            //xlWs.get_Range("A6 : A7").Merge();    //Cell[6,1], Cell[8,1]
            //xlWs.get_Range("B6 : B7").Merge();    //Cell[6,2], Cell[8,2]
            //xlWs.get_Range("C6 : H6").Merge();    //Cell[6,3], Cell[8,3]
            //xlWs.get_Range("I6 : N6").Merge();    
            //xlWs.get_Range("A1 : N1").Merge();    
            
            // membuat merge cell - 2
            xlWs.Range[xlWs.Cells[6, 1], xlWs.Cells[7, 1]].Merge();
            xlWs.Range[xlWs.Cells[6, 2], xlWs.Cells[7, 2]].Merge();
            xlWs.Range[xlWs.Cells[6, 3], xlWs.Cells[6, 8]].Merge();
            xlWs.Range[xlWs.Cells[6, 9], xlWs.Cells[6, 14]].Merge();
            
            // mengatur width dan height
            xlWs.get_Range("B6").EntireColumn.ColumnWidth = 40;
            xlWs.get_Range("A39 : N39").EntireRow.RowHeight = 30;
            
            // mengatur warna shade
            xlWs.get_Range("A6 : N7").Cells.Interior.Color = Color.AliceBlue;
            
            // mengatur allign text dan huruf
            xlWs.get_Range("A6 : N7").Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            xlWs.get_Range("A1 : N1").Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            xlWs.get_Range("A6 : N7").Cells.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
            xlWs.get_Range("A1 : N1").Cells.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
            xlWs.get_Range("A8 : A2000").Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            xlWs.get_Range("A39 : N39").Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            xlWs.get_Range("A39 : N39").Cells.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
            xlWs.get_Range("I8 : I2000").HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            xlWs.get_Range("J8 : J2000").HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            xlWs.get_Range("K8 : K2000").HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            xlWs.get_Range("L8 : L2000").HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            xlWs.get_Range("M8 : M2000").HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            xlWs.get_Range("N8 : N2000").HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

            xlWs.get_Range("I8 : I2000").VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            xlWs.get_Range("J8 : J2000").VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            xlWs.get_Range("K8 : K2000").VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            xlWs.get_Range("L8 : L2000").VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            xlWs.get_Range("M8 : M2000").VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            xlWs.get_Range("N8 : N2000").VerticalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            
            // mengatur font jadi bold
            xlWs.get_Range("A6 : N7").Font.Bold = true;
            xlWs.get_Range("A39 : N39").Font.Bold = true;
            xlWs.get_Range("A1 : N1").Font.Bold = true;
            xlWs.get_Range("A1 : N1").Font.Size = 14;
            
            // Mengatur number format
            xlWs.get_Range("I8 : I2000").NumberFormat = "###,####%";
            xlWs.get_Range("J8 : J2000").NumberFormat = "###,####%";
            xlWs.get_Range("K8 : K2000").NumberFormat = "###,####%";
            xlWs.get_Range("L8 : L2000").NumberFormat = "###,####%";
            xlWs.get_Range("M8 : M2000").NumberFormat = "###,####%";
            xlWs.get_Range("N8 : N2000").NumberFormat = "###,####%";

            // memberi nama header
            xlWs.Cells[1, 1] = Cmb_AsalSekolah.Text;
            xlWs.Cells[6, 1] = "NO";
            xlWs.Cells[6, 2] = "Kategori";
            xlWs.Cells[6, 3] = "Jumlah data dari sesi Peneleponan ke";
            xlWs.Cells[7, 3] = "1";
            xlWs.Cells[7, 4] = "2";
            xlWs.Cells[7, 5] = "3";
            xlWs.Cells[7, 6] = "4";
            xlWs.Cells[7, 7] = "5";
            xlWs.Cells[7, 8] = "Total";
            xlWs.Cells[6, 9] = "Persentase Per Sesi";
            xlWs.Cells[7, 9] = "1";
            xlWs.Cells[7, 10] = "2";
            xlWs.Cells[7, 11] = "3";
            xlWs.Cells[7, 12] = "4";
            xlWs.Cells[7, 13] = "5";
            xlWs.Cells[7, 14] = "Total";


            //Isi dengan datagrid
            try
            {
                //Hasil Angka
                for (i = 0; i < dg2.Rows.Count; i++)
                {
                    for (j = 0; j < dg2.Columns.Count; j++)
                    {
                        if (i <= dg2.Rows.Count && dg2.Rows[i].Cells[j].Value != null)
                            xlWs.Cells[i + 8, j + 1] = dg2.Rows[i].Cells[j].Value.ToString();
                    }
                }
                //Hasil Persentase seluruh dari datagrid
                for (i = 0; i < dg3.Rows.Count; i++)
                {
                    for (j = 0; j < dg3.Columns.Count; j++)
                    {
                        if (i <= dg3.Rows.Count && dg3.Rows[i].Cells[j].Value != null)
                        {
                            for (j = 2; j < dg3.Columns.Count; j++)
                            {
                                if (i <= dg3.Rows.Count)
                                {
                                    if (float.Parse(dg3.Rows[i].Cells[j].Value.ToString()) != 0)
                                    {
                                        float nilaiPerCell = float.Parse(String.Format("{0:0.0000}", float.Parse(dg3.Rows[i].Cells[j].Value.ToString())));
                                        xlWs.Cells[i + 8, j + 7] = nilaiPerCell;
                                    }
                                    else
                                    {
                                        xlWs.Cells[i + 8, j + 7] = " 0% ";
                                    }
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }

        private void Buat_Grafik()
        {
            for (int p = 0; p <= 6; p++)
            {
                 //set grafik
                 //arahin ke workshit nomor 2
                xlWs = (Excel.Worksheet)xlWb.Worksheets.get_Item(3);

                 //kasih nama dan warna worksheet 2
                xlWs.Name = "Grafik";
                xlWs.Tab.Color = Color.Orange;
                // Kasih nama di cell
                xlWs.get_Range("A1 : N1").Merge();
                xlWs.get_Range("A1 : N1").Font.Size = 14;
                xlWs.get_Range("A1 : N1").Font.Bold = true;
                xlWs.get_Range("A1 : N1").Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                xlWs.get_Range("A1 : N1").Cells.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                xlWs.Cells[1, 1] = "Grafik " + Cmb_AsalSekolah.Text;
                 //pembuatan grafik
                if (p == 0)
                {
                    // chart Batang
                    var charts = xlWs.ChartObjects() as Microsoft.Office.Interop.Excel.ChartObjects;
                    var chartObject = charts.Add(60, 80, 750, 750) as Microsoft.Office.Interop.Excel.ChartObject;
                    var chart = chartObject.Chart;
                    xlWs = (Excel.Worksheet)xlWb.Worksheets.get_Item(2);
                    var range = xlWs.get_Range("A6 : H38");
                    chart.SetSourceData(range);

                    chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered;
                    chart.ChartWizard(Source: range,
                        Title: "Statistik Data Sesi Peneleponan Per Sekolah",
                        CategoryTitle: "Hasil Peneleponan",
                        ValueTitle: "Jumlah Data");
                   
                }
                else if ( p == 1 )
                {
                    //var charts = xlWs.ChartObjects() as Microsoft.Office.Interop.Excel.ChartObjects;
                    //var chartObject = charts.Add(60, 900, 750, 750) as Microsoft.Office.Interop.Excel.ChartObject;
                    //var chart = chartObject.Chart;
                    //xlWs = (Excel.Worksheet)xlWb.Worksheets.get_Item(2);
                    //var range = xlWs.get_Range("B7 : B38 ; I7 : I38");
                    //chart.SetSourceData(range);
                    //chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlPie;
                    //chart.ChartTitle.Text = "Persentase Peneleponan Sesi 1 ";
                    //chart.ApplyDataLabels(Excel.XlDataLabelsType.xlDataLabelsShowPercent);

                    MakingChart(60, 900, 750, 750, "I7", "I38", "B7", "B38", "Persentase Peneleponan Sesi 1");
                }
                else if (p == 2)
                {
                    //var charts = xlWs.ChartObjects() as Microsoft.Office.Interop.Excel.ChartObjects;
                    //var chartObject = charts.Add(60, 1700, 750, 750) as Microsoft.Office.Interop.Excel.ChartObject;
                    //var chart = chartObject.Chart;
                    //xlWs = (Excel.Worksheet)xlWb.Worksheets.get_Item(2);
                    //var range = xlWs.get_Range("B7 : B38 ; J7 : J38");
                    //chart.SetSourceData(range);
                    //chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlPie;
                    //chart.ChartTitle.Text = "Persentase Peneleponan Sesi 2 ";
                    //chart.ApplyDataLabels(Excel.XlDataLabelsType.xlDataLabelsShowPercent);

                    MakingChart(60, 1700, 750, 750, "J7", "J38", "B7", "B38", "Persentase Peneleponan Sesi 2");
                }
                else if (p == 3)
                {
                    //var charts = xlWs.ChartObjects() as Microsoft.Office.Interop.Excel.ChartObjects;
                    //var chartObject = charts.Add(60, 2500, 750, 750) as Microsoft.Office.Interop.Excel.ChartObject;
                    //var chart = chartObject.Chart;
                    //xlWs = (Excel.Worksheet)xlWb.Worksheets.get_Item(2);
                    //var range = xlWs.get_Range("B7 : B38 ; K7 : K38");
                    //chart.SetSourceData(range);
                    //chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlPie;
                    //chart.ChartTitle.Text = "Persentase Peneleponan Sesi 3 ";
                    //chart.ApplyDataLabels(Excel.XlDataLabelsType.xlDataLabelsShowPercent);

                    MakingChart(60, 2500, 750, 750, "K7", "K38", "B7", "B38", "Persentase Peneleponan Sesi 3");
                }
                else if (p == 4)
                {
                    //var charts = xlWs.ChartObjects() as Microsoft.Office.Interop.Excel.ChartObjects;
                    //var chartObject = charts.Add(60, 3300, 750, 750) as Microsoft.Office.Interop.Excel.ChartObject;
                    //var chart = chartObject.Chart;
                    //xlWs = (Excel.Worksheet)xlWb.Worksheets.get_Item(2);
                    //var range = xlWs.get_Range("B7 : B38 ; L7 : L38");
                    //chart.SetSourceData(range);
                    //chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlPie;
                    //chart.ChartTitle.Text = "Persentase Peneleponan Sesi 4 ";
                    //chart.ApplyDataLabels(Excel.XlDataLabelsType.xlDataLabelsShowPercent);

                    MakingChart(60, 3300, 750, 750, "L7", "L38", "B7", "B38", "Persentase Peneleponan Sesi 4");
                }
                else if (p == 5)
                {
                    //var charts = xlWs.ChartObjects() as Microsoft.Office.Interop.Excel.ChartObjects;
                    //var chartObject = charts.Add(60, 4100, 750, 750) as Microsoft.Office.Interop.Excel.ChartObject;
                    //var chart = chartObject.Chart;
                    //xlWs = (Excel.Worksheet)xlWb.Worksheets.get_Item(2);
                    //var range = xlWs.get_Range("B7 : B38 ; M7 : M38");
                    //chart.SetSourceData(range);
                    //chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlPie;
                    //chart.ChartTitle.Text = "Persentase Peneleponan Sesi 5 ";
                    //chart.ApplyDataLabels(Excel.XlDataLabelsType.xlDataLabelsShowPercent);

                    MakingChart(60, 4100, 750, 750, "M7", "M38", "B7", "B38", "Persentase Peneleponan Sesi 5");
                }
                else if (p == 6)
                {
                    //var charts = xlWs.ChartObjects() as Microsoft.Office.Interop.Excel.ChartObjects;
                    //var chartObject = charts.Add(60, 4900, 750, 750) as Microsoft.Office.Interop.Excel.ChartObject;
                    //var chart = chartObject.Chart;
                    //xlWs = (Excel.Worksheet)xlWb.Worksheets.get_Item(2);
                    //var range = xlWs.get_Range("B7 : B38 ; N7 : N38");
                    //chart.SetSourceData(range);
                    //chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlPie;
                    //chart.ChartTitle.Text = "Persentase Peneleponan Seluruh Sesi ";
                    //chart.ApplyDataLabels(Excel.XlDataLabelsType.xlDataLabelsShowPercent);

                    MakingChart(60, 4900, 750, 750, "N7", "N38", "B7", "B38", "Persentase Peneleponan Seluruh Sesi");
                }
            }
        }

        private void MakingChart(double x, double y, double w, double h, string nilaiR1, string nilaiR2, 
            string catR1, string catR2, string textTitle)
        {
            object missValue = System.Reflection.Missing.Value;

            Excel.ChartObjects charts = (Excel.ChartObjects)xlWs.ChartObjects(missValue);
            Excel.ChartObject chartObject = charts.Add(x, y, w, h) as Excel.ChartObject;
            Excel.Chart chart = chartObject.Chart;
            xlWs = (Excel.Worksheet)xlWb.Worksheets.get_Item(2);

            // range untuk nilai - input range
            var range = xlWs.get_Range(nilaiR1 + ":" + nilaiR2);
            
            // input range ke source data
            chart.SetSourceData(range);

            // chart type
            chart.ChartType = Excel.XlChartType.xlPie;

            // x categories
            Excel.Axis axis = (Excel.Axis)chart.Axes(Excel.XlAxisType.xlCategory, Excel.XlAxisGroup.xlPrimary);
            Excel.Range rangeAxis = xlWs.get_Range(catR1, catR2);

            // implement axis
            axis.CategoryNames = rangeAxis;

            // chart title
            chart.HasTitle = true;
            chart.ChartTitle.Text = textTitle;
            chart.ApplyDataLabels(Excel.XlDataLabelsType.xlDataLabelsShowPercent);
        }

        private void Save_Dialog()
        {
            Sfd_ExportPersekolah.InitialDirectory = "D:";
            Sfd_ExportPersekolah.Title = "Save as Excel File";
            Sfd_ExportPersekolah.FileName = "ReportSesiPeneleponan " + Cmb_AsalSekolah.Text + " tahun " + Cmb_TahunPromosi.Text;
            //Sfd_ExportPersekolah.Filter = "Excel Files(2003)|*.xls|Excel Files(2010)|*.xlsx";
            Sfd_ExportPersekolah.Filter = "Excel Files(2010)|*.xlsx";
            if (Sfd_ExportPersekolah.ShowDialog() != DialogResult.Cancel)
            {
                //try
                //{
                    Export_Excel_AllData(DGView_AllData);
                    Export_Excel_Hasil(DGView_Hasil, DGView_Persentase);
                    Buat_Grafik();
                //}
                //catch (Exception ex)
                //{
                //    MessageBox.Show(ex.Message);
                //}
                xlApp.ActiveWorkbook.SaveCopyAs(Sfd_ExportPersekolah.FileName.ToString());
                xlApp.ActiveWorkbook.Saved = true;
                xlApp.Quit();

                releaseObject(xlWs);
                releaseObject(xlWb);
                releaseObject(xlApp);

                MessageBox.Show("Excel file created");
            }
            else
            {

            }
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        private void Btn_Export_Click(object sender, EventArgs e)
        {
            Save_Dialog();
        }

        public static bool isChartAppeared = false;
        private void Btn_Chart_Click(object sender, EventArgs e)
        {
            if(!isChartAppeared)
            {
                isChartAppeared = !isChartAppeared;
                Form_Chart_Report_Persekolah formChart = new Form_Chart_Report_Persekolah();
                formChart.Show();
                formChart.MdiParent = this.MdiParent;
            }
        }

        private void Form_Report_Persekolah_FormClosing(object sender, FormClosingEventArgs e)
        {
            Main.isChildFormAppeared = !Main.isChildFormAppeared;
        }

        private void Cmb_TahunPromosi_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillCmbAsalSekolah();
            Cmb_AsalSekolah.Enabled = true;
        }
    }
}
