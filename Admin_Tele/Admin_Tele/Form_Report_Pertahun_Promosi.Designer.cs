﻿namespace Admin_Tele
{
    partial class Form_Report_Pertahun_Promosi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.Cmb_TahunPromo = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Btn_Chart = new System.Windows.Forms.Button();
            this.Btn_Export = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Sfd_ExportPersekolah = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(6, 19);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(1248, 509);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.Cmb_TahunPromo);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.Btn_Chart);
            this.groupBox2.Controls.Add(this.Btn_Export);
            this.groupBox2.Location = new System.Drawing.Point(940, 552);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(332, 104);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            // 
            // Cmb_TahunPromo
            // 
            this.Cmb_TahunPromo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_TahunPromo.FormattingEnabled = true;
            this.Cmb_TahunPromo.Location = new System.Drawing.Point(139, 19);
            this.Cmb_TahunPromo.Name = "Cmb_TahunPromo";
            this.Cmb_TahunPromo.Size = new System.Drawing.Size(183, 21);
            this.Cmb_TahunPromo.TabIndex = 1;
            this.Cmb_TahunPromo.SelectedIndexChanged += new System.EventHandler(this.Cmb_TahunPromo_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(127, 16);
            this.label2.TabIndex = 22;
            this.label2.Text = "Pilih Tahun Promosi";
            // 
            // Btn_Chart
            // 
            this.Btn_Chart.Enabled = false;
            this.Btn_Chart.Location = new System.Drawing.Point(234, 60);
            this.Btn_Chart.Name = "Btn_Chart";
            this.Btn_Chart.Size = new System.Drawing.Size(92, 36);
            this.Btn_Chart.TabIndex = 3;
            this.Btn_Chart.Text = "Tampilkan Grafik";
            this.Btn_Chart.UseVisualStyleBackColor = true;
            this.Btn_Chart.Click += new System.EventHandler(this.Btn_Chart_Click);
            // 
            // Btn_Export
            // 
            this.Btn_Export.Enabled = false;
            this.Btn_Export.Location = new System.Drawing.Point(143, 60);
            this.Btn_Export.Name = "Btn_Export";
            this.Btn_Export.Size = new System.Drawing.Size(84, 37);
            this.Btn_Export.TabIndex = 2;
            this.Btn_Export.Text = "Export to Excel";
            this.Btn_Export.UseVisualStyleBackColor = true;
            this.Btn_Export.Click += new System.EventHandler(this.Btn_Export_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.dataGridView1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1260, 534);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            // 
            // Form_Report_Pertahun_Promosi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Admin_Tele.Properties.Resources.images__5_;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1284, 668);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "Form_Report_Pertahun_Promosi";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Report Pertahun Promosi";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Report_Pertahun_Promosi_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox Cmb_TahunPromo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button Btn_Chart;
        private System.Windows.Forms.Button Btn_Export;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.SaveFileDialog Sfd_ExportPersekolah;
    }
}