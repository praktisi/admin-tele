﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Admin_Tele
{
    public partial class DistribusiSekolah : Form
    {
        string query;
        List<string> namaSekolah = new List<string>();
        int banyakSekolah = 0;
        List<string> updateSekolah = new List<string>();

        MySqlConnection conn = new MySqlConnection(Constanta.conString);
        MySqlCommand cmd;
        MySqlDataAdapter adpt;

        public static string pil_TahunPromo;

        public DistribusiSekolah()
        {
            InitializeComponent();
            FillTahunPromo();
        }

        private void FillTahunPromo()
        {
            query = "SELECT DISTINCT tahun_promosi FROM tb_dataangket";
            cmd = new MySqlCommand(query, conn);

            try
            {
                conn.Open();
                DataTable dt = new DataTable();
                adpt = new MySqlDataAdapter(cmd);
                adpt.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    Cmb_TahunPromo.Items.Add(r[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        bool isEnable = false;

        private void FillNamaMarketing()
        {
            isEnable = false;

            query = "SELECT * FROM tb_marketing";
            cmd = new MySqlCommand(query, conn);

            try
            {
                conn.Open();
                DataSet ds = new DataSet();
                adpt = new MySqlDataAdapter(cmd);
                adpt.Fill(ds);

                Cmb_NamaMarketing.ValueMember = "id_marketing";
                Cmb_NamaMarketing.DisplayMember = "nama_marketing";
                Cmb_NamaMarketing.DataSource = ds.Tables[0];

                Cmb_NamaMarketing.Text = null;
                Cmb_NamaTele.Text = null;

                conn.Close();

                isEnable = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void GetNamaSekolah(string idMar)
        {
            namaSekolah.Clear();

            query = "SELECT DISTINCT asal_sekolah FROM tb_dataangket WHERE id_marketing ='" + idMar + "'";
            cmd = new MySqlCommand(query, conn);

            try
            {
                conn.Open();
                DataTable dt = new DataTable();
                adpt = new MySqlDataAdapter(cmd);
                adpt.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    namaSekolah.Add(r[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void FillNamaTele(string idMar)
        {
            query = "SELECT * FROM tb_telemarketing WHERE id_marketing = '" + idMar + "'";
            cmd = new MySqlCommand(query, conn);

            try
            {
                conn.Open();
                DataSet ds = new DataSet();
                adpt = new MySqlDataAdapter(cmd);
                adpt.Fill(ds);

                Cmb_NamaTele.ValueMember = "id_telemarketing";
                Cmb_NamaTele.DisplayMember = "nama_telemarketing";
                Cmb_NamaTele.DataSource = ds.Tables[0];

                Cmb_NamaTele.Text = null;

                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ResetAndClearCmb()
        {
            Cmb_TahunPromo.Text = null;
            Cmb_NamaMarketing.Enabled = false;
            Cmb_NamaTele.Enabled = false;
            Cmb_NamaMarketing.Text = null;
            Cmb_NamaTele.Text = null;
            isEnable = false;
            banyakSekolah = 0;
            updateSekolah.Clear();
        }

        private void BikinChekBoxByCount(int jml)
        {
            Panel_DaftarSekolah.Controls.Clear();

            int xChkBox = 3;
            int yChkBox = 11;

            int jedaYBox = 26;

            for (int i = 0; i < jml; i++)
            {
                CheckBox ChkBox = new CheckBox();
                ChkBox.Location = new Point(xChkBox, (i * jedaYBox) + yChkBox);
                ChkBox.Name = "Chkbox_NamaSekolah" + i.ToString();
                ChkBox.AutoSize = true;
                ChkBox.Text = namaSekolah[i];
                ChkBox.CheckedChanged += new System.EventHandler(chkBox_Checked);

                query = "SELECT id_telemarketing FROM tb_dataangket WHERE asal_sekolah = '" + namaSekolah[i] + "'";
                cmd = new MySqlCommand(query, conn);

                try
                {
                    conn.Open();
                    MySqlDataReader dReader;

                    dReader = cmd.ExecuteReader();

                    if(dReader.Read() && dReader[0].ToString() != "")
                    {
                        ChkBox.Enabled = false;
                    }
                    conn.Close();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                Panel_DaftarSekolah.Controls.Add(ChkBox);
            }
        }

        void chkBox_Checked(object sender, EventArgs e)
        {
            var cb = (CheckBox)sender;

            if (cb.Checked == true)
            {
                banyakSekolah += 1;
                updateSekolah.Add(cb.Text);
            }
            else
            {
                banyakSekolah -= 1;
                updateSekolah.Remove(cb.Text);
            }

            if (cb.Checked == true && Cmb_NamaTele.Text != "")
            {
                Btn_Submit.Enabled = true;
            }

            if(banyakSekolah == 0)
            {
                Btn_Submit.Enabled = false;
            }
        }

        private void UpdateIdTele(string idTel, string asalSklh)
        {
            query = "UPDATE tb_dataangket SET id_telemarketing = '" + idTel + "' WHERE asal_sekolah = '" + asalSklh + "'";
            cmd = new MySqlCommand(query, conn);

            try
            {
                conn.Open();

                adpt = new MySqlDataAdapter(cmd);
                adpt.UpdateCommand = conn.CreateCommand();
                adpt.UpdateCommand.CommandText = query;

                if (cmd.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show(asalSklh + " telah di tambahkan di list telemarketing " + Cmb_NamaTele.Text);
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                conn.Close();
            }
        }

        private void Cmb_TahunPromo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Cmb_TahunPromo.Text != "")
            {
                Cmb_NamaMarketing.Enabled = false;
                Cmb_NamaTele.Enabled = false;
                FillNamaMarketing();
                Cmb_NamaMarketing.Enabled = true;
                pil_TahunPromo = Cmb_TahunPromo.Text;
            }
        }

        private void Cmb_NamaMarketing_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Cmb_NamaMarketing.Text != "" && isEnable)
            {
                FillNamaTele(Cmb_NamaMarketing.SelectedValue.ToString());

                GetNamaSekolah(Cmb_NamaMarketing.SelectedValue.ToString());

                BikinChekBoxByCount(namaSekolah.Count);

                Cmb_NamaTele.Enabled = true;
            }
            else
            {
                Panel_DaftarSekolah.Controls.Clear();
            }
        }

        private void Btn_Submit_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < banyakSekolah; i++)
            {
                UpdateIdTele(Cmb_NamaTele.SelectedValue.ToString(), updateSekolah[i]);
            }

            ResetAndClearCmb();
        }

        private void Cmb_NamaTele_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (banyakSekolah > 0)
            {
                Btn_Submit.Enabled = true;  
            }
            else
            {
                Btn_Submit.Enabled = false;
            }
        }

        private void DistribusiSekolah_FormClosing(object sender, FormClosingEventArgs e)
        {
            Main.isChildFormAppeared = !Main.isChildFormAppeared;
        }       
    }
}
