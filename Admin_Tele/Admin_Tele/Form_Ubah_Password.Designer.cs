﻿namespace Admin_Tele
{
    partial class Form_Ubah_Password
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GrpBox_KonfirmPassword = new System.Windows.Forms.GroupBox();
            this.Btn_Konfirmasi = new System.Windows.Forms.Button();
            this.TxtBox_Password = new System.Windows.Forms.TextBox();
            this.TxtBox_Username = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Btn_Ubah = new System.Windows.Forms.Button();
            this.Btn_Batal = new System.Windows.Forms.Button();
            this.GrpBox_UbahPassword = new System.Windows.Forms.GroupBox();
            this.TxtBox_PwdBaru = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.GrpBox_KonfirmPassword.SuspendLayout();
            this.GrpBox_UbahPassword.SuspendLayout();
            this.SuspendLayout();
            // 
            // GrpBox_KonfirmPassword
            // 
            this.GrpBox_KonfirmPassword.BackColor = System.Drawing.Color.Transparent;
            this.GrpBox_KonfirmPassword.Controls.Add(this.Btn_Konfirmasi);
            this.GrpBox_KonfirmPassword.Controls.Add(this.TxtBox_Password);
            this.GrpBox_KonfirmPassword.Controls.Add(this.TxtBox_Username);
            this.GrpBox_KonfirmPassword.Controls.Add(this.label2);
            this.GrpBox_KonfirmPassword.Controls.Add(this.label1);
            this.GrpBox_KonfirmPassword.Location = new System.Drawing.Point(12, 12);
            this.GrpBox_KonfirmPassword.Name = "GrpBox_KonfirmPassword";
            this.GrpBox_KonfirmPassword.Size = new System.Drawing.Size(304, 117);
            this.GrpBox_KonfirmPassword.TabIndex = 0;
            this.GrpBox_KonfirmPassword.TabStop = false;
            this.GrpBox_KonfirmPassword.Text = "Masukkan Username dan Password anda";
            // 
            // Btn_Konfirmasi
            // 
            this.Btn_Konfirmasi.Location = new System.Drawing.Point(125, 87);
            this.Btn_Konfirmasi.Name = "Btn_Konfirmasi";
            this.Btn_Konfirmasi.Size = new System.Drawing.Size(75, 23);
            this.Btn_Konfirmasi.TabIndex = 4;
            this.Btn_Konfirmasi.Text = "Konfirmasi";
            this.Btn_Konfirmasi.UseVisualStyleBackColor = true;
            this.Btn_Konfirmasi.Click += new System.EventHandler(this.Btn_Konfirmasi_Click);
            // 
            // TxtBox_Password
            // 
            this.TxtBox_Password.Location = new System.Drawing.Point(83, 61);
            this.TxtBox_Password.Name = "TxtBox_Password";
            this.TxtBox_Password.PasswordChar = '*';
            this.TxtBox_Password.Size = new System.Drawing.Size(204, 20);
            this.TxtBox_Password.TabIndex = 3;
            // 
            // TxtBox_Username
            // 
            this.TxtBox_Username.Location = new System.Drawing.Point(83, 30);
            this.TxtBox_Username.Name = "TxtBox_Username";
            this.TxtBox_Username.Size = new System.Drawing.Size(204, 20);
            this.TxtBox_Username.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Password";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Username";
            // 
            // Btn_Ubah
            // 
            this.Btn_Ubah.Location = new System.Drawing.Point(106, 47);
            this.Btn_Ubah.Name = "Btn_Ubah";
            this.Btn_Ubah.Size = new System.Drawing.Size(75, 23);
            this.Btn_Ubah.TabIndex = 5;
            this.Btn_Ubah.Text = "Ubah";
            this.Btn_Ubah.UseVisualStyleBackColor = true;
            this.Btn_Ubah.Click += new System.EventHandler(this.Btn_Ubah_Click);
            // 
            // Btn_Batal
            // 
            this.Btn_Batal.Location = new System.Drawing.Point(187, 48);
            this.Btn_Batal.Name = "Btn_Batal";
            this.Btn_Batal.Size = new System.Drawing.Size(75, 23);
            this.Btn_Batal.TabIndex = 6;
            this.Btn_Batal.Text = "Batal";
            this.Btn_Batal.UseVisualStyleBackColor = true;
            this.Btn_Batal.Click += new System.EventHandler(this.Btn_Batal_Click);
            // 
            // GrpBox_UbahPassword
            // 
            this.GrpBox_UbahPassword.BackColor = System.Drawing.Color.Transparent;
            this.GrpBox_UbahPassword.Controls.Add(this.Btn_Batal);
            this.GrpBox_UbahPassword.Controls.Add(this.Btn_Ubah);
            this.GrpBox_UbahPassword.Controls.Add(this.TxtBox_PwdBaru);
            this.GrpBox_UbahPassword.Controls.Add(this.label3);
            this.GrpBox_UbahPassword.Location = new System.Drawing.Point(12, 135);
            this.GrpBox_UbahPassword.Name = "GrpBox_UbahPassword";
            this.GrpBox_UbahPassword.Size = new System.Drawing.Size(304, 77);
            this.GrpBox_UbahPassword.TabIndex = 5;
            this.GrpBox_UbahPassword.TabStop = false;
            this.GrpBox_UbahPassword.Text = "Massukan Password Baru Anda";
            this.GrpBox_UbahPassword.Visible = false;
            // 
            // TxtBox_PwdBaru
            // 
            this.TxtBox_PwdBaru.Location = new System.Drawing.Point(83, 21);
            this.TxtBox_PwdBaru.Name = "TxtBox_PwdBaru";
            this.TxtBox_PwdBaru.PasswordChar = '*';
            this.TxtBox_PwdBaru.Size = new System.Drawing.Size(204, 20);
            this.TxtBox_PwdBaru.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 16);
            this.label3.TabIndex = 1;
            this.label3.Text = "Password";
            // 
            // Form_Ubah_Password
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Admin_Tele.Properties.Resources.Background2;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(328, 222);
            this.Controls.Add(this.GrpBox_UbahPassword);
            this.Controls.Add(this.GrpBox_KonfirmPassword);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "Form_Ubah_Password";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ubah Password";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Ubah_Password_FormClosing);
            this.GrpBox_KonfirmPassword.ResumeLayout(false);
            this.GrpBox_KonfirmPassword.PerformLayout();
            this.GrpBox_UbahPassword.ResumeLayout(false);
            this.GrpBox_UbahPassword.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GrpBox_KonfirmPassword;
        private System.Windows.Forms.Button Btn_Batal;
        private System.Windows.Forms.Button Btn_Ubah;
        private System.Windows.Forms.Button Btn_Konfirmasi;
        private System.Windows.Forms.TextBox TxtBox_Password;
        private System.Windows.Forms.TextBox TxtBox_Username;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox GrpBox_UbahPassword;
        private System.Windows.Forms.TextBox TxtBox_PwdBaru;
        private System.Windows.Forms.Label label3;
    }
}