﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Admin_Tele
{
    public partial class TelemarketingManager : Form
    {
        MySqlConnection conn = new MySqlConnection(Constanta.conString);
        MySqlCommand cmd;
        MySqlDataAdapter adpt;
        MySqlDataReader dReader;
        string query;

        public TelemarketingManager()
        {
            InitializeComponent();

            DesignDataGrid();
            ShowData();
            FillMarketingLeader();
        }

        private void DesignDataGrid()
        {
            dataGridView1.ColumnCount = 5;
            dataGridView1.Columns[0].Name = "ID Telemarketing";
            dataGridView1.Columns[1].Name = "Nama Telemarketing";
            dataGridView1.Columns[1].Width = 175;
            dataGridView1.Columns[2].Name = "Username";
            dataGridView1.Columns[2].Width = 175;
            dataGridView1.Columns[3].Name = "Password";
            dataGridView1.Columns[3].Width = 100;
            dataGridView1.Columns[4].Name = "ID Marketing";

            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.AllowUserToResizeColumns = false;
        }

        private void FillMarketingLeader()
        {
            query = "SELECT * FROM tb_marketing";
            cmd = new MySqlCommand(query, conn);

            try
            {
                conn.Open();
                DataSet ds = new DataSet();
                adpt = new MySqlDataAdapter(cmd);
                adpt.Fill(ds);

                Cmb_MarketingLeader.ValueMember = "id_marketing";
                Cmb_MarketingLeader.DisplayMember = "nama_marketing";
                Cmb_MarketingLeader.DataSource = ds.Tables[0];

                conn.Close();

                ClearText();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void FillTextBox()
        {
            string idTel = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            string namaTel = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            string uname = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
            string pwd = dataGridView1.SelectedRows[0].Cells[3].Value.ToString();
            string idMar = dataGridView1.SelectedRows[0].Cells[4].Value.ToString();

            Cmb_MarketingLeader.SelectedValue = idMar;
            TxtBox_IDTele.Text = idTel;
            TxtBox_NamaTele.Text = namaTel;
            TxtBox_Password.Text = pwd;
            TxtBox_Username.Text = uname;
        }

        string GetCurrentIDFromDB()
        {
            conn.Close();
            string calonID = "";
            string idTerakhir = "";

            query = "SELECT id_telemarketing FROM tb_telemarketing ORDER BY id_telemarketing DESC LIMIT 0,1";
            cmd = new MySqlCommand(query, conn);

            try
            {
                conn.Open();
                dReader = cmd.ExecuteReader();

                if (dReader.Read())
                {
                    idTerakhir = dReader[0].ToString();
                    
                    int i = int.Parse(idTerakhir);

                    int newID = i + 1;

                    calonID = idTerakhir.Substring(0, 3 - newID.ToString().Length) + newID;
                }
                else
                {
                    calonID = "001";
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return calonID;
        }

        private void ShowData()
        {
            dataGridView1.Rows.Clear();

            query = "SELECT * FROM tb_telemarketing";
            cmd = new MySqlCommand(query, conn);

            try
            {
                conn.Open();
                DataTable dt = new DataTable();
                adpt = new MySqlDataAdapter(cmd);
                adpt.Fill(dt);

                foreach(DataRow r in dt.Rows)
                {
                    dataGridView1.Rows.Add(r[0].ToString(), r[1].ToString(), r[2].ToString(), r[3].ToString(), r[4].ToString());
                }

                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ClearText()
        {
            Cmb_MarketingLeader.Text = null;
            TxtBox_IDMarketing.Text = "";
            TxtBox_IDTele.Text = "";
            TxtBox_NamaTele.Text = "";
            TxtBox_Password.Text = "";
            TxtBox_Username.Text = "";

            GrpBox_DataTele.Enabled = false;
            Btn_Batal.Enabled = false;
            Btn_Hapus.Enabled = false;
            Btn_Input.Enabled = false;
            Btn_Ubah.Enabled = false;
        }

        private void InputData()
        {
            bool isUsernameAda = false;
            query = "SELECT username_telemarketing FROM tb_telemarketing";
            cmd = new MySqlCommand(query, conn);

            try
            {
                conn.Open();

                DataTable dt = new DataTable();
                adpt = new MySqlDataAdapter(cmd);
                adpt.Fill(dt);
                foreach(DataRow r in dt.Rows)
                {
                    if(r[0].ToString() == TxtBox_Username.Text)
                    {
                        isUsernameAda = true;
                        MessageBox.Show("Maaf username dengan nama " + TxtBox_Username.Text + " sudah ada, harap mencari username lain");
                    }
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            if(!isUsernameAda)
            {
                query = "INSERT INTO tb_telemarketing(id_telemarketing, nama_telemarketing, username_telemarketing, password_telemarketing, id_marketing)  VALUES(@idTel, @namaTel, @uname, @pwd, @idMar)";
                cmd = new MySqlCommand(query, conn);

                cmd.Parameters.AddWithValue("@idTel", TxtBox_IDTele.Text);
                cmd.Parameters.AddWithValue("@namaTel", TxtBox_NamaTele.Text);
                cmd.Parameters.AddWithValue("@uname", TxtBox_Username.Text);
                cmd.Parameters.AddWithValue("@pwd", TxtBox_Password.Text);
                cmd.Parameters.AddWithValue("@idMar", TxtBox_IDMarketing.Text);

                try
                {
                    conn.Open();

                    if (cmd.ExecuteNonQuery() > 0)
                    {
                        MessageBox.Show("Marketing " + TxtBox_NamaTele.Text + " telah berhasil ditambahkan ke list telemarketing");
                    }

                    conn.Close();


                    ShowData();
                    ClearText();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void UbahData(string idTel, string namaTel, string uname, string pwd, string idMar)
        {
            bool isUsernameAda = false;
            query = "SELECT username_telemarketing FROM tb_telemarketing";
            cmd = new MySqlCommand(query, conn);

            try
            {
                conn.Open();

                DataTable dt = new DataTable();
                adpt = new MySqlDataAdapter(cmd);
                adpt.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    if (r[0].ToString() == TxtBox_Username.Text && TxtBox_Username.Text != dataGridView1.SelectedRows[0].Cells[2].Value.ToString())
                    {
                        isUsernameAda = true;
                        MessageBox.Show("Maaf anda tidak bisa mengubah username anda menjadi " + TxtBox_Username.Text + 
                            " karena username tersebut sudah digunakan, harap mencari username lain");
                    }
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            if (!isUsernameAda)
            {
                query = "UPDATE tb_telemarketing SET nama_telemarketing='" + namaTel + "', username_telemarketing='" + uname + "', password_telemarketing='" + pwd + "', id_marketing='" + idMar + "' WHERE id_telemarketing = '" + idTel + "'";
                cmd = new MySqlCommand(query, conn);

                try
                {
                    conn.Open();

                    adpt = new MySqlDataAdapter(cmd);
                    adpt.UpdateCommand = conn.CreateCommand();
                    adpt.UpdateCommand.CommandText = query;

                    if (cmd.ExecuteNonQuery() > 0)
                    {
                        MessageBox.Show("Data telemarketing " + namaTel + " telah berhasil diubah");
                    }
                    conn.Close();

                    ShowData();
                    ClearText();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void HapusData(string idTel)
        {
            query = "DELETE FROM tb_telemarketing WHERE id_telemarketing = '" + idTel + "'";
            cmd = new MySqlCommand(query, conn);

            try
            {
                conn.Open();

                adpt = new MySqlDataAdapter(cmd);
                adpt.UpdateCommand = conn.CreateCommand();
                adpt.UpdateCommand.CommandText = query;

                if (cmd.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("Data telemarketing " + TxtBox_NamaTele.Text + " telah berhasil dihapus dari list telemarketing");
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Cmb_MarketingLeader_SelectedValueChanged(object sender, EventArgs e)
        {
            if (Cmb_MarketingLeader.SelectedValue != null && TxtBox_IDTele.Text == "")
            {
                TxtBox_IDMarketing.Text = Cmb_MarketingLeader.SelectedValue.ToString();
                TxtBox_IDTele.Text = GetCurrentIDFromDB();
                GrpBox_DataTele.Enabled = true;
                Btn_Input.Enabled = true;
                TxtBox_NamaTele.Focus();
            }

            else if (Cmb_MarketingLeader.SelectedValue != null &&  TxtBox_IDTele.Text != GetCurrentIDFromDB())
            {
                TxtBox_IDMarketing.Text = Cmb_MarketingLeader.SelectedValue.ToString();
                GrpBox_DataTele.Enabled = true;
                Btn_Input.Enabled = false;
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            FillTextBox();

            Btn_Input.Enabled = false;
            Btn_Batal.Enabled = true;
            Btn_Hapus.Enabled = true;
            Btn_Ubah.Enabled = true;
        }

        private void Btn_Batal_Click(object sender, EventArgs e)
        {
            ClearText();
        }

        private void Btn_Input_Click(object sender, EventArgs e)
        {
            InputData();
        }

        private void Btn_Ubah_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Apakah anda akin akan mengubah data telemarketing " + TxtBox_NamaTele.Text + " ?",
                    "Ubah Data", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                UbahData(TxtBox_IDTele.Text, TxtBox_NamaTele.Text, TxtBox_Username.Text, TxtBox_Password.Text, TxtBox_IDMarketing.Text);
            }
        }

        private void Btn_Hapus_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Apakah anda yakin akan menghapus data telemarketing " + TxtBox_NamaTele.Text +" ?",
                    "Hapus Data", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                HapusData(TxtBox_IDTele.Text);
                ShowData();
                ClearText();
            }
        }

        private void TelemarketingManager_FormClosing(object sender, FormClosingEventArgs e)
        {
            Main.isChildFormAppeared = !Main.isChildFormAppeared;
        }
    }
}
