﻿namespace Admin_Tele
{
    partial class FormReportPerTelemarketing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Cmb_TahunPromosi = new System.Windows.Forms.ComboBox();
            this.Btn_Chart = new System.Windows.Forms.Button();
            this.Cmb_NamaTelemarketing = new System.Windows.Forms.ComboBox();
            this.Btn_Export = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Sfd_ExportPersekolah = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(6, 19);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(1248, 436);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.Cmb_TahunPromosi);
            this.groupBox2.Controls.Add(this.Btn_Chart);
            this.groupBox2.Controls.Add(this.Cmb_NamaTelemarketing);
            this.groupBox2.Controls.Add(this.Btn_Export);
            this.groupBox2.Location = new System.Drawing.Point(908, 479);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(364, 133);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(127, 16);
            this.label2.TabIndex = 18;
            this.label2.Text = "Pilih Tahun Promosi";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(163, 16);
            this.label1.TabIndex = 16;
            this.label1.Text = "Pilih Nama Telemarketing";
            // 
            // Cmb_TahunPromosi
            // 
            this.Cmb_TahunPromosi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_TahunPromosi.FormattingEnabled = true;
            this.Cmb_TahunPromosi.Location = new System.Drawing.Point(175, 19);
            this.Cmb_TahunPromosi.Name = "Cmb_TahunPromosi";
            this.Cmb_TahunPromosi.Size = new System.Drawing.Size(183, 21);
            this.Cmb_TahunPromosi.TabIndex = 1;
            this.Cmb_TahunPromosi.SelectedIndexChanged += new System.EventHandler(this.Cmb_TahunPromosi_SelectedIndexChanged);
            // 
            // Btn_Chart
            // 
            this.Btn_Chart.Enabled = false;
            this.Btn_Chart.Location = new System.Drawing.Point(222, 91);
            this.Btn_Chart.Name = "Btn_Chart";
            this.Btn_Chart.Size = new System.Drawing.Size(92, 36);
            this.Btn_Chart.TabIndex = 4;
            this.Btn_Chart.Text = "Tampilkan Grafik";
            this.Btn_Chart.UseVisualStyleBackColor = true;
            this.Btn_Chart.Click += new System.EventHandler(this.Btn_Chart_Click);
            // 
            // Cmb_NamaTelemarketing
            // 
            this.Cmb_NamaTelemarketing.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_NamaTelemarketing.Enabled = false;
            this.Cmb_NamaTelemarketing.FormattingEnabled = true;
            this.Cmb_NamaTelemarketing.Location = new System.Drawing.Point(175, 55);
            this.Cmb_NamaTelemarketing.Name = "Cmb_NamaTelemarketing";
            this.Cmb_NamaTelemarketing.Size = new System.Drawing.Size(183, 21);
            this.Cmb_NamaTelemarketing.TabIndex = 2;
            this.Cmb_NamaTelemarketing.SelectedIndexChanged += new System.EventHandler(this.Cmb_NamaTelemarketing_SelectedIndexChanged);
            // 
            // Btn_Export
            // 
            this.Btn_Export.Enabled = false;
            this.Btn_Export.Location = new System.Drawing.Point(58, 90);
            this.Btn_Export.Name = "Btn_Export";
            this.Btn_Export.Size = new System.Drawing.Size(84, 37);
            this.Btn_Export.TabIndex = 3;
            this.Btn_Export.Text = "Export to Excel";
            this.Btn_Export.UseVisualStyleBackColor = true;
            this.Btn_Export.Click += new System.EventHandler(this.Btn_Export_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.dataGridView1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1260, 461);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            // 
            // FormReportPerTelemarketing
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Admin_Tele.Properties.Resources.images__5_;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1284, 668);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "FormReportPerTelemarketing";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Report Pertelemarketing";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormReportPerTelemarketing_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button Btn_Chart;
        private System.Windows.Forms.ComboBox Cmb_NamaTelemarketing;
        private System.Windows.Forms.Button Btn_Export;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox Cmb_TahunPromosi;
        private System.Windows.Forms.SaveFileDialog Sfd_ExportPersekolah;
    }
}