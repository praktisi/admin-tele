﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Admin_Tele
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
            MDIBackgroundImage();
        }

        //set background main form
        private void MDIBackgroundImage()
        {
            foreach (Control ctl in this.Controls)
            {
                if (ctl is MdiClient)
                {
                    ctl.BackgroundImage = Properties.Resources.background1;
                }
            }
        }

        public static bool isChildFormAppeared = false;

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void perSekolahToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!isChildFormAppeared)
            {
                isChildFormAppeared = !isChildFormAppeared;

                Form_Report_Persekolah FormReportPersekolah = new Form_Report_Persekolah();
                FormReportPersekolah.MdiParent = this;
                FormReportPersekolah.Show();
            }
        }

        private void perMarketingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!isChildFormAppeared)
            {
                isChildFormAppeared = !isChildFormAppeared;

                FormReportPerMarketing FormReportPermarketing = new FormReportPerMarketing();
                FormReportPermarketing.MdiParent = this;
                FormReportPermarketing.Show();
            }
        }

        private void perTelemarketingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!isChildFormAppeared)
            {
                isChildFormAppeared = !isChildFormAppeared;

                FormReportPerTelemarketing FormReportPertele = new FormReportPerTelemarketing();
                FormReportPertele.MdiParent = this;
                FormReportPertele.Show();
            }
        }

        private void perTahunPromosiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!isChildFormAppeared)
            {
                isChildFormAppeared = !isChildFormAppeared;

                Form_Report_Pertahun_Promosi FormReportPertahun = new Form_Report_Pertahun_Promosi();
                FormReportPertahun.MdiParent = this;
                FormReportPertahun.Show();
            }
        }

        private void logoutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Anda yakin akan kembali ke halaman login?", "Log Out",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                Login login = new Login();
                login.Show();
                this.Hide();
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!isChildFormAppeared)
            {
                isChildFormAppeared = !isChildFormAppeared;

                DistribusiSekolah bagiSklh = new DistribusiSekolah();
                bagiSklh.MdiParent = this;
                bagiSklh.Show();
            }
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!isChildFormAppeared)
            {
                isChildFormAppeared = !isChildFormAppeared;

                Form_Ubah_Password gantiPwd = new Form_Ubah_Password();
                gantiPwd.MdiParent = this;
                gantiPwd.Show();
            }
        }

        private void telemarketingManagerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!isChildFormAppeared)
            {
                isChildFormAppeared = !isChildFormAppeared;

                TelemarketingManager teleManagerForm = new TelemarketingManager();
                teleManagerForm.MdiParent = this;
                teleManagerForm.Show();
            }
        }

        private void distribusiTelemarketingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!isChildFormAppeared)
            {
                isChildFormAppeared = !isChildFormAppeared;

                Form_Marketing_Leader formMarketing = new Form_Marketing_Leader();
                formMarketing.MdiParent = this;
                formMarketing.Show();
            }
        }

        public static bool isUserManualAppeared = false;
        private void caraPenggunaanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!isUserManualAppeared)
            {
                isUserManualAppeared = !isUserManualAppeared;

                User_Manual userManual = new User_Manual();
                userManual.Show();
            }
        }

        public static bool isAboutAppeared = false;
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(!isAboutAppeared)
            {
                isAboutAppeared = !isAboutAppeared;

                Form_About about = new Form_About();
                about.ShowDialog();
            }
        }

        private void smsSiswa_Click(object sender, EventArgs e)
        {
            if (!isChildFormAppeared)
            {
                isChildFormAppeared = !isChildFormAppeared;
                
                SmsGatewaySiswa sgs = new SmsGatewaySiswa();
                sgs.MdiParent = this;
                sgs.Show();
            }
        }

        private void caraPenggunaanToolStripMenuItem_Click_1(object sender, EventArgs e)
        {

        }
    }
}
