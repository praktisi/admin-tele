﻿namespace Admin_Tele
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.formToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.distribusiTelemarketingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.telemarketingManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.reportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.perSekolahToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.perTelemarketingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.perMarketingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.perTahunPromosiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.topSekolahToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sMSBlastToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.smsSiswa = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.caraPenggunaanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.formToolStripMenuItem,
            this.reportToolStripMenuItem,
            this.sMSBlastToolStripMenuItem,
            this.helpToolStripMenuItem});
            resources.ApplyResources(this.menuStrip1, "menuStrip1");
            this.menuStrip1.Name = "menuStrip1";
            // 
            // formToolStripMenuItem
            // 
            this.formToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.distribusiTelemarketingToolStripMenuItem,
            this.telemarketingManagerToolStripMenuItem,
            this.exitToolStripMenuItem,
            this.logoutToolStripMenuItem,
            this.logoutToolStripMenuItem1});
            this.formToolStripMenuItem.Name = "formToolStripMenuItem";
            resources.ApplyResources(this.formToolStripMenuItem, "formToolStripMenuItem");
            // 
            // distribusiTelemarketingToolStripMenuItem
            // 
            this.distribusiTelemarketingToolStripMenuItem.Name = "distribusiTelemarketingToolStripMenuItem";
            resources.ApplyResources(this.distribusiTelemarketingToolStripMenuItem, "distribusiTelemarketingToolStripMenuItem");
            this.distribusiTelemarketingToolStripMenuItem.Click += new System.EventHandler(this.distribusiTelemarketingToolStripMenuItem_Click);
            // 
            // telemarketingManagerToolStripMenuItem
            // 
            this.telemarketingManagerToolStripMenuItem.Name = "telemarketingManagerToolStripMenuItem";
            resources.ApplyResources(this.telemarketingManagerToolStripMenuItem, "telemarketingManagerToolStripMenuItem");
            this.telemarketingManagerToolStripMenuItem.Click += new System.EventHandler(this.telemarketingManagerToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            resources.ApplyResources(this.exitToolStripMenuItem, "exitToolStripMenuItem");
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            resources.ApplyResources(this.logoutToolStripMenuItem, "logoutToolStripMenuItem");
            this.logoutToolStripMenuItem.Click += new System.EventHandler(this.logoutToolStripMenuItem_Click);
            // 
            // logoutToolStripMenuItem1
            // 
            this.logoutToolStripMenuItem1.Name = "logoutToolStripMenuItem1";
            resources.ApplyResources(this.logoutToolStripMenuItem1, "logoutToolStripMenuItem1");
            this.logoutToolStripMenuItem1.Click += new System.EventHandler(this.logoutToolStripMenuItem1_Click);
            // 
            // reportToolStripMenuItem
            // 
            this.reportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.perSekolahToolStripMenuItem,
            this.perTelemarketingToolStripMenuItem,
            this.perMarketingToolStripMenuItem,
            this.perTahunPromosiToolStripMenuItem,
            this.topSekolahToolStripMenuItem});
            this.reportToolStripMenuItem.Name = "reportToolStripMenuItem";
            resources.ApplyResources(this.reportToolStripMenuItem, "reportToolStripMenuItem");
            // 
            // perSekolahToolStripMenuItem
            // 
            this.perSekolahToolStripMenuItem.Name = "perSekolahToolStripMenuItem";
            resources.ApplyResources(this.perSekolahToolStripMenuItem, "perSekolahToolStripMenuItem");
            this.perSekolahToolStripMenuItem.Click += new System.EventHandler(this.perSekolahToolStripMenuItem_Click);
            // 
            // perTelemarketingToolStripMenuItem
            // 
            this.perTelemarketingToolStripMenuItem.Name = "perTelemarketingToolStripMenuItem";
            resources.ApplyResources(this.perTelemarketingToolStripMenuItem, "perTelemarketingToolStripMenuItem");
            this.perTelemarketingToolStripMenuItem.Click += new System.EventHandler(this.perTelemarketingToolStripMenuItem_Click);
            // 
            // perMarketingToolStripMenuItem
            // 
            this.perMarketingToolStripMenuItem.Name = "perMarketingToolStripMenuItem";
            resources.ApplyResources(this.perMarketingToolStripMenuItem, "perMarketingToolStripMenuItem");
            this.perMarketingToolStripMenuItem.Click += new System.EventHandler(this.perMarketingToolStripMenuItem_Click);
            // 
            // perTahunPromosiToolStripMenuItem
            // 
            this.perTahunPromosiToolStripMenuItem.Name = "perTahunPromosiToolStripMenuItem";
            resources.ApplyResources(this.perTahunPromosiToolStripMenuItem, "perTahunPromosiToolStripMenuItem");
            this.perTahunPromosiToolStripMenuItem.Click += new System.EventHandler(this.perTahunPromosiToolStripMenuItem_Click);
            // 
            // topSekolahToolStripMenuItem
            // 
            this.topSekolahToolStripMenuItem.Name = "topSekolahToolStripMenuItem";
            resources.ApplyResources(this.topSekolahToolStripMenuItem, "topSekolahToolStripMenuItem");
            // 
            // sMSBlastToolStripMenuItem
            // 
            this.sMSBlastToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.smsSiswa});
            this.sMSBlastToolStripMenuItem.Name = "sMSBlastToolStripMenuItem";
            resources.ApplyResources(this.sMSBlastToolStripMenuItem, "sMSBlastToolStripMenuItem");
            // 
            // smsSiswa
            // 
            this.smsSiswa.Name = "smsSiswa";
            resources.ApplyResources(this.smsSiswa, "smsSiswa");
            this.smsSiswa.Click += new System.EventHandler(this.smsSiswa_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem,
            this.caraPenggunaanToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            resources.ApplyResources(this.helpToolStripMenuItem, "helpToolStripMenuItem");
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            resources.ApplyResources(this.aboutToolStripMenuItem, "aboutToolStripMenuItem");
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // caraPenggunaanToolStripMenuItem
            // 
            this.caraPenggunaanToolStripMenuItem.Name = "caraPenggunaanToolStripMenuItem";
            resources.ApplyResources(this.caraPenggunaanToolStripMenuItem, "caraPenggunaanToolStripMenuItem");
            this.caraPenggunaanToolStripMenuItem.Click += new System.EventHandler(this.caraPenggunaanToolStripMenuItem_Click_1);
            // 
            // Main
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Main";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem formToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem distribusiTelemarketingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem telemarketingManagerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem perSekolahToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem perTelemarketingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem perMarketingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem perTahunPromosiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem topSekolahToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem caraPenggunaanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem sMSBlastToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem smsSiswa;
    }
}