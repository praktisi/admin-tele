﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;

namespace Admin_Tele
{
    class RegClass
    {
        private string subkey = "SOFTWARE\\" + Application.ProductName;

        public string SubKey { get { return subkey; } }

        /// <summary>
        /// Return value from registry if keyName is exist
        /// </summary>
        /// <param name="keyName"></param>
        /// <returns></returns>
        public string ReadFromRegistry(string keyName)
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey(SubKey);

            if( key == null)
            {
                return null;
            }
            else
            {
                try
                {
                    return (string)key.GetValue(keyName.ToUpper());
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    return null;
                }
            }
        }

        /// <summary>
        /// Writing registry keyname with object value
        /// </summary>
        /// <param name="keyName">string for key on registry</param>
        /// <param name="val">value of</param>
        /// <returns></returns>
        public bool WriteToRegistry(string keyName, object val)
        {
            try
            {
                RegistryKey key = Registry.CurrentUser.CreateSubKey(SubKey);
                key.SetValue(keyName, val);
                key.Close();

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("can't write : " + ex.Message);
                return false;
            }
        }
    }
}
