﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Admin_Tele
{
    public partial class FormSettings : Form
    {
        MySqlConnection connect = new MySqlConnection(Constanta.conString);
        MySqlConnection connectionTesting;

        RegClass regClass = new RegClass();

        public FormSettings()
        {
            InitializeComponent();
        }
        private void Cek_Koneksi(TextBox t)
        {
            connectionTesting = new MySqlConnection("Server=" + t.Text + "; Database=db_marketingpraktisi; Uid=root; Pwd=");

            try
            {
                connectionTesting.Open();

                MessageBox.Show("Connect");

                connectionTesting.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR CONNECT: " + ex.Message);
            }
        }
        private bool ValidasiIP()
        {
            bool b = false;

            string ip = txtbox_IpAddress.Text;
            string digit6Awal = "";

            if (ip.Length > 7)
            {
                digit6Awal = ip.Substring(0, 8);

                if (digit6Awal == "192.168.")
                {
                    b = true;
                }
                else
                {
                    b = false;
                }
            }
            return b;
        }

        private void btn_SetIP_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Yakin Untuk Set Alamat " + txtbox_IpAddress.Text + " sebagai Server Baru?", "Set IP",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (regClass.WriteToRegistry("ip", txtbox_IpAddress.Text))
                {
                    Constanta.server = txtbox_IpAddress.Text;
                    Constanta.SettingServer();
                    MessageBox.Show("Server " + Constanta.server + " Berhasil di Save");
                    txtbox_IpAddress.Clear();
                }
            }
        }

        private void btn_CekKoneksi_Click(object sender, EventArgs e)
        {
            if (!ValidasiIP())
                MessageBox.Show("Isi terlebih dahulu Server IP Address");
            else
            {
                Cek_Koneksi(txtbox_IpAddress);
            }
        }

        
    }
}
